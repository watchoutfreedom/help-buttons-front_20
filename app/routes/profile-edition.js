import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { resolve, hash } from 'rsvp';
import { get } from '@ember/object';

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),

    model() {
      let currentUser = this.session.currentUser;
      currentUser = currentUser.constructor.toString() === 'DS.PromiseObject' ?
        currentUser : resolve(currentUser);
      return currentUser.then((user) => {
        const avatar = get(user, 'avatar');
        const interests = get(user, 'interests');
        const selectedLanguages = get(user, 'languages');
        const languages = this.store.findAll('language');
        return hash({
          currentUser,
          avatar,
          interests,
          languages,
          selectedLanguages,
        });
      });
    },
  }
);
