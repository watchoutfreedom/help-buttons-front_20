import { computed } from '@ember/object';
import { isNone } from '@ember/utils';
let currentUserCache;

export default function currentUser(sessionServicePropertyName = 'session') {
  function needsToQueryUserData(instance) {
    return instance.get(`${sessionServicePropertyName}.isAuthenticated`) &&
      isNone(currentUserCache);
  }

  function needsToInvalidateUser(instance) {
    return !instance.get(`${sessionServicePropertyName}.isAuthenticated`);
  }

  return computed(`${sessionServicePropertyName}.isAuthenticated`, function() {
    if (needsToQueryUserData(this)) {
      currentUserCache = this.store.findRecord('user', 'current');
    } else if (needsToInvalidateUser(this)) {
      currentUserCache = null;
    }

    return currentUserCache;
  }).volatile();
}
