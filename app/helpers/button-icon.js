import { helper } from '@ember/component/helper';
import { isEmpty } from '@ember/utils';
import { htmlSafe } from '@ember/string';

// const sgrLogo = '<img src="/assets/images/housing-makers-logo.png" class="search-map__marker-logo-housing-makers">';

/**
  * this helper is used to generate the div icons for each marker of the leaflet map.
  * It takes two parameters, a number, relevant data for the estate, and a string
  * the URL of the logo to show in the div icon
  *
  * @param estate {DS.model.Estate} the estate to show its data in the map
  *
  * @return {HtmlSafeString} Html img element in string format
  */
export function buttonIcon([ button, avatar ]) {
  const avatarThumbUrl = isEmpty(avatar) || isEmpty(avatar.content) ? null : avatar.content.tinyThumb;
  const imgUrl = isEmpty(avatarThumbUrl) ? '/assets/svg/logo/icon.svg' : avatarThumbUrl;
  let htmlOutput;
  switch (button) {
    case 'desde':
      htmlOutput = `
        <figure class="button-icon button-icon--offer">
          <img src=${imgUrl} class="button-icon__image "/>
          <span class="button-icon__arrow"></span>
        </figure>
      `;
      break;
    case 'hasta':
      htmlOutput = `
        <figure class="button-icon button-icon--need">
          <img src=${imgUrl} class="button-icon__image "/>
          <span class="button-icon__arrow"></span>
        </figure>
      `;
      break;
    default:
      if (button.swap) {
        htmlOutput = `
          <figure class="button-icon button-icon--swap">
            <img src=${imgUrl} class="button-icon__image "/>
            <span class="button-icon__arrow"></span>
          </figure>
        `;
      } else {
        htmlOutput = `
          <figure class="button-icon button-icon--${button.buttonType}">
            <img src=${imgUrl} class="button-icon__image "/>
            <span class="button-icon__arrow"></span>
          </figure>
        `;
      }
  }
  return htmlSafe(htmlOutput);
}

export default helper(buttonIcon);
