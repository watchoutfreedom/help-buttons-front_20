import Component from '@ember/component';
import { resolve } from 'rsvp';
import { or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed, set } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  isShowingModal: false,
  section: null,
  firstLoad: false,

  didInsertElement() {
    if (!isEmpty(this.section) && this.firstLoad === true) {
      this._super(...arguments);
      set(this, 'firstLoad', false);
      if (this.section === 'donate') {
        document.getElementById('showBeHelper').scrollIntoView();
      }
    }
  },

  showWhatIsHelpButtons: computed('section', function() {
    if (this.section === 'info') {
      document.getElementById('showWhatIsHelpButtons').scrollIntoView();
      return true;
    }
    return false;
  }),
  showWhyHelpButtons: computed('section', function() {
    if (this.section === 'why') {
      document.getElementById('showWhyHelpButtons').scrollIntoView();
      return true;
    }
    return false;
  }),
  showEasyToUse: computed('section', function() {
    if (this.section === 'easy-use') {
      document.getElementById('easy-showEasyToUse').scrollIntoView();
      return true;
    }
    return false;
  }),
  showCanDo: computed('section', function() {
    if (this.section === 'can-do') {
      document.getElementById('can-showCanDo').scrollIntoView();
      return true;
    }
    return false;
  }),
  showImagination: computed('section', function() {
    if (this.section === 'imagination') {
      document.getElementById('showImagination').scrollIntoView();
      return true;
    }
    return false;
  }),
  showHowItWorks: computed('section', function() {
    if (this.section === 'how-it-works') {
      document.getElementById('how-it-showHowItWorks').scrollIntoView();
      return true;
    }
    return false;
  }),
  showCreateCollaborativeButton: computed('section', function() {
    if (this.section === 'create') {
      document.getElementById('showCreateCollaborativeButton').scrollIntoView();
      return true;
    }
    return false;
  }),
  showHowIsAButton: computed('section', function() {
    if (this.section === 'how-is') {
      document.getElementById('how-showHowIsAButton').scrollIntoView();
      return true;
    }
    return false;
  }),
  showBeHelper: computed('section', function() {
    if (this.section === 'donate') {
      document.getElementById('showBeHelper').scrollIntoView();
      set(this, 'firstLoad', true);
      return true;
    }
    return false;
  }),
  showCollaborate: computed('section', function() {
    if (this.section === 'collaborate') {
      document.getElementById('showCollaborate').scrollIntoView();
      return true;
    }
    return false;
  }),
  showBehind: computed('section', function() {
    if (this.section === 'about') {
      document.getElementById('showBehind').scrollIntoView();
      return true;
    }
    return false;
  }),
  showPolicyAndConventions: computed('section', function() {
    if (this.section === 'policy') {
      document.getElementById('showPolicyAndConventions').scrollIntoView();
      return true;
    }
    return false;
  }),
  showPrivacyPolicy: computed('section', function() {
    if (this.section === 'privacy') {
      document.getElementById('showPrivacyPolicy').scrollIntoView();
      return true;
    }
    return false;
  }),
  showSecuritySuggestions: computed('section', function() {
    if (this.section === 'security') {
      document.getElementById('showSecuritySuggestions').scrollIntoView();
      return true;
    }
    return false;
  }),
  showGeneralRules: computed('section', function() {
    if (this.section === 'general-rules') {
      document.getElementById('general-showGeneralRules').scrollIntoView();
      return true;
    }
    return false;
  }),
  showCategories: computed('section', function() {
    if (this.section === 'categories') {
      document.getElementById('showCategories').scrollIntoView();
      return true;
    }
    return false;
  }),
  showContact: computed('section', function() {
    if (this.section === 'contact') {
      document.getElementById('showContact').scrollIntoView();
      return true;
    }
    return false;
  }),
  showRepresent: computed('section', function() {
    if (this.section === 'represent') {
      document.getElementById('showRepresent').scrollIntoView();
      return true;
    }
    return false;
  }),
  showCollaborateApps: computed('section', function() {
    if (this.section === 'collaborateApps') {
      document.getElementById('showCollaborateApps').scrollIntoView();
      return true;
    }
    return false;
  }),


  createButtonLink: computed('isSmallDevice', function() {
    return this.isSmallDevice ? 'buttons.new' : 'index.index.new-button';
  }),

  searchByAddress() {},


  actions: {
    searchByAddress(address, tags) {
      this.searchByAddress(address, tags);
    },

    toggleProperty(param) {
      this.toggleProperty(param);
    },
  },
});
