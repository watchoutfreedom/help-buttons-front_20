import Component from '@ember/component';
import { computed, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { notEmpty } from '@ember/object/computed';
import { inject as service } from '@ember/service';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Component.extend({
  classNames: ['col-12 card-notification button-file__messages'],
  notifications: service('notification-messages'),
  chat: null,
  user: computed(
    'chat',
    {
      get() {
        this.chat.user.then((user) => {
          set(this, 'user', user);
        });
      },
      set(key, value) {
        return value;
      },
    }
  ),
  hasUserAvatar: notEmpty('userAvatar'),

  userAvatar: computed(
    'user',
    {
      get() {
        if (!isEmpty(this.user)) {
          this.user.avatar.then((userAvatar) => {
            set(this, 'userAvatar', userAvatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  actions: {
    removeChat() {
      this.chat.deleteRecord();
      this.chat.save()
        .then(() => {
          this.notifications.success(
            'Chat eliminado correctamente',
            NOTIFICATION_OPTIONS,
          );
          this.toggleProperty('showConfirmation');
        })
        .catch(() => {
          this.notifications.success(
            'Error al eleminar el chat',
            NOTIFICATION_OPTIONS,
          );
        });
    },
    showConfirmationDialog() {
      this.toggleProperty('showConfirmation');
    },
  },
});
