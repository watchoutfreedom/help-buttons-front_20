import Component from '@ember/component';
import { notEmpty } from '@ember/object/computed';

export default Component.extend({
  errors: null,
  classNames: ['input'],
  classNameBindings: ['hasError:input--error'],

  /**
   * Class name binding when its true the 'is-invalid' class
   * it's added to tag component
   *
   * @property has-error
   * @type Boolean
   */
  'hasError': notEmpty('errors'),
});
