import Mixin from '@ember/object/mixin';
import { get, set, getProperties } from '@ember/object';
import { readOnly } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { hash, resolve } from 'rsvp';
import { isEmpty } from '@ember/utils';

export default Mixin.create({
  session: service(),
  currentUser: readOnly('session.currentUser'),

  canceledAction: false,

  actions: {
    nextStep(canceledAction) {
      set(this, 'canceledAction', canceledAction);
      if (this.session.isAuthenticated) {
        const button = this.model;
        const currentUser = this.currentUser;
        const { offerTags, neededTags } = getProperties(button, 'offerTags', 'neededTags');

        button.save()
          .then((savedButton) => {
            let image = resolve();
            if (!isEmpty(button.imageFile)) {
              const options = {};
              options.headers = {
                'Authorization': this.session.data.authenticated.token,
              };
              options.data = {
                name: button.imageFile.name,
                file: button.imageFile,
                'imageable-id': this.model.id,
                'imageable-type': 'button',
                'imageable-property': 'image',
              };

              image = button.imageFile.upload('/api/v1/images', options)
                .then((data) => {
                  const normalizedImageResponse = this.store.normalize('image', data.body.data);
                  const newImage = this.store.push(normalizedImageResponse);
                  set(button, 'image', newImage);
                  return newImage;
                });
            }
            return hash({
              currentUserResponse: currentUser,
              ownedButtons: get(currentUser, 'ownedButtons'),
              offerTags: isEmpty(offerTags) ? resolve() : savedButton.updateRelationship('offerTags'),
              neededTags: isEmpty(neededTags) ? resolve() : savedButton.updateRelationship('neededTags'),
              image,
            });
          })
          .then(({ currentUserResponse, ownedButtons }) => {
            let ownedButtonsCounter;
            if (isEmpty(currentUserResponse)) {
              ownedButtonsCounter = this.currentUser;
            } else {
              ownedButtonsCounter = get(currentUserResponse, 'ownedButtonsCounter');
            }

            set(currentUserResponse, 'ownedButtonsCounter', ownedButtonsCounter + 1);
            ownedButtons.pushObject(button);
            if (this.canceledAction) {
              const selfRoute = this.activationRoute.substr(0, this.activationRoute.lastIndexOf('.'));
              this.transitionToRoute(selfRoute + '.location');
            } else {
              this.transitionToRoute(this.activationRoute);
            }
          });
      } else {
        this.transitionToRoute(this.activationRoute);
      }
    },

    updateRelatedType(image) {
      this.model.pushObject(image);
    },
  },
});
