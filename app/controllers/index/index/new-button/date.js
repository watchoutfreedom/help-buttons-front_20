import Controller from '@ember/controller';
import DateStepMixin from '../../../../mixins/date-step-mixin';

export default Controller.extend(
  DateStepMixin,
  {
    activationRoute: 'index.index.new-button.activate-button',
    actions: {
      closeAction() {
        this.transitionToRoute('index.index');
      },
    },
  }
);
