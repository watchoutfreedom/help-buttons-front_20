import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    nextStep() {
      this.transitionToRoute('buttons.new.date');
    },
  },
});
