import { reject } from 'rsvp';

export default Object.extend({
  open: function(auth) {
    if (!auth.code) {
      return reject();
    }

    localStorage.token = auth.code;
    const adapter = this.container.lookup('adapter:application');
    adapter.set('headers', { 'Authorization': localStorage.token });

    return this.get('store').find('user', 'me')
      .then(function(user) {
        return {
          currentUser: user,
        };
      });
  },
  fetch: function() {
    if (!localStorage.token) {
      return reject();
    }

    const adapter = this.container.lookup('adapter:application');
    adapter.set('headers', { 'Authorization': localStorage.token });

    return this.get('store').find('user', 'me')
      .then(function(user) {
        return {
          currentUser: user,
        };
      });
  },
});
