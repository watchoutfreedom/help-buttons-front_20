import Route from '@ember/routing/route';
import { set } from '@ember/object';

export default Route.extend({

  queryParams: {
    searchTags: {
      refreshModel: true,
      as: 'searchTags',
    },
  },

  deactivate() {
    set(this.controller, 'selectedButton', null);
    set(this.controller, 'showProfile', null);
  },
});
