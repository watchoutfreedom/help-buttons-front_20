import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { or, alias } from '@ember/object/computed';
import { computed, get, set } from '@ember/object';
import { resolve } from 'rsvp';
import { htmlSafe } from '@ember/string';
import { isEmpty } from '@ember/utils';

import ButtonsFiltersMixin from '../mixins/buttons-filters-mixin';

export default Controller.extend(
  ButtonsFiltersMixin,
  {
    // SERVICES
    mediaQueries: service(),
    routing: service('-routing'),
    geolocation: service('geolocation'),

    // QUERY PARAMS
    queryParams: [
      'latitude',
      'longitude',
      'northEastLat',
      'northEastLng',
      'southWestLat',
      'southWestLng',
      'address',
      'hideWelcome',
      'userMarker',
      'buttonsDisplay',
      'searchTags',
    ],

    /**
     * query param, used to performs searchs by point
     * @type {Number}
     * @property latitude & longitude
     */
    latitude: null,
    longitude: null,
    /**
     * query param, used to performs searchs by area
     * @type {Number}
     * @property northEastLat & northEastLng & southWestLat & southWestLng
     */
    northEastLat: null,
    northEastLng: null,
    southWestLat: null,
    southWestLng: null,
    address: null,
    userMarker: false,
    hideWelcome: null,
    buttonsDisplay: null,
    searchTags: null,

    // COMPUTED PROPERTIES
    isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

    isShowingMenuModal: false,

    isShowingModal: false,

    /**
     * Contains all buttons without filters applied yet, this CP
     * is used from buttons-filters component and ButtonsFiltersMixin
     * @type {Array}
     */
    buttons: alias('model.buttons'),

    currentUser: alias('model.currentUser'),

    userPosition: null,

    lastUserLat: null,
    lastUserLng: null,

    isMapRoute: computed(
      'routing.router.currentURL',
      function() {
        return !get(this, 'routing.router.currentURL').includes('buttonsDisplay=list');
      }
    ),

    showOnlyCreate: computed(
      'isSmallDevice',
      'hideWelcome',
      function() {
        if (get(this, 'hideWelcome') === 'false') {
          return get(this, 'isSmallDevice');
        }
        return false;
      }
    ),

    asideStyles: computed(
      'selectedTags.[]',
      'buttonsDisplay',
      function() {
        get(this, 'tagsToApply');
        if (this.buttonsDisplay === 'list') {
          return '';
        }
        const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
        const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
        const filtersHeight = document.getElementById('buttonsFiltersId').clientHeight;
        const headerSectionHeight = headerHeight + searchTopBarHeight + filtersHeight + 15;
        const asideStyles = `height: ${window.innerHeight - headerSectionHeight}px;`;
        return htmlSafe(asideStyles);
      }
    ),

    loading: false,

    actions: {
      searchByAddress(address) {
        const urlParams = new URLSearchParams(window.location.search);
        let newTags = null;
        if (!isEmpty(urlParams.get('searchTags'))) {
          newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
        }


        const queryParams = {
          latitude: null,
          longitude: null,
          address,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: null,
          searchTags: newTags,
        };
        return resolve().then(() => {
          this.transitionToRoute('index', { queryParams });
        });
      },

      localizeUser() {
        if(get(this, 'userMarker')) {
          const queryParams = {
            latitude: get(this, 'userPosition.lat'),
            longitude: get(this, 'userPosition.lng'),
            address: null,
            northEastLat: null,
            northEastLng: null,
            southWestLat: null,
            southWestLng: null,
            userMarker: false,
          };
          this.transitionToRoute('index', { queryParams });
          return null;
        }
        const geolocation = get(this, 'geolocation');
        return geolocation.getCurrentPosition()
          .then((coordinates) => {
            const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);

            if (isEmpty(get(this, 'lastUserLat')) || isEmpty(get(this, 'lastUserLng'))) {
              set(this, 'lastUserLat', userPosition.lat);
              set(this, 'lastUserLng', userPosition.lng);
              const queryParams = {
                latitude: userPosition.lat,
                longitude: userPosition.lng,
                address: null,
                northEastLat: null,
                northEastLng: null,
                southWestLat: null,
                southWestLng: null,
                userMarker: true,
              };
              this.transitionToRoute('index', { queryParams });
            } else {
              set(this, 'userPosition', userPosition);
              const maxPosLat = userPosition.lat > get(this, 'lastUserLat') ?
                userPosition.lat : get(this, 'lastUserLat');
              const minPosLat = userPosition.lat > get(this, 'lastUserLat') ?
                get(this, 'lastUserLat') : userPosition.lat;

              const maxPosLng = userPosition.lng > get(this, 'lastUserLng') ?
                userPosition.lng : get(this, 'lastUserLng');
              const minPosLng = userPosition.lng > get(this, 'lastUserLng') ?
                get(this, 'lastUserLng') : userPosition.lng;
              if ((maxPosLat - minPosLat > 0.0005) || (maxPosLng - minPosLng > 0.0005)) {
                set(this, 'lastUserLat', userPosition.lat);
                set(this, 'lastUserLng', userPosition.lng);
                const queryParams = {
                  latitude: get(this, 'lastUserLat'),
                  longitude: get(this, 'lastUserLng'),
                  address: null,
                  northEastLat: null,
                  northEastLng: null,
                  southWestLat: null,
                  southWestLng: null,
                  userMarker: true,
                };
                this.transitionToRoute('index', { queryParams });
              }
            }
          });
      },
      loginTransition() {
        const queryParams = {
          latitude: null,
          longitude: null,
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
        };
        this.transitionToRoute('index', { queryParams });
      },
    },
  }
);
