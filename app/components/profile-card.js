import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { notEmpty } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import { set, get } from '@ember/object';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Component.extend({
  classNames: ['profile-card'],

  notifications: service('notification-messages'),
  user: null,
  avatar: null,

  isOwnProfile: false,

  buttonsRoute: computed(
    'isOwnProfile',
    function() {
      return this.isOwnProfile ? 'profile.buttons' : 'user.buttons';
    },
  ),

  languagesString: computed('user.languages.@each.name', function() {
    let languages = '';
    if (!isEmpty(get(this, 'user.languages'))) {
      for (let i = 0; i < get(this, 'user.languages').length; i++) {
        if (!isEmpty(get(this, 'user.languages').toArray()[i].name)) {
          languages = languages + ' #' + get(this, 'user.languages').toArray()[i].name;
        }
      }
    }
    return languages;
  }),

  hasLocation: notEmpty('user.location'),
  hasLanguages: notEmpty('languagesString'),

  locationAndLanguages: computed('user.{location}', 'languagesString', function() {
    if (this.hasLanguages) {
      if (this.hasLocation) {
        return get(this, 'user.location') + ' - ' + this.languagesString;
      }
      return this.languagesString;
    }
    if (this.hasLocation) {
      return get(this, 'user.languages');
    }
    return '';
  }),

  closeAction() {},
  profileClick: false,

  actions: {
    closeModal() {
      if (!this.profileClick) {
        this.closeAction();
      }
      set(this, 'profileClick', false);
    },
    profileClick() {
      set(this, 'profileClick', true);
    },

    toggleProfileState() {
      this.toggleProperty('user.active');
      return this.user.save().then((user) => {
        if (user.active) {
          this.notifications.success(
            'Has activado tu usuario',
            NOTIFICATION_OPTIONS,
          );
        } else {
          this.notifications.info(
            'Has desactivado tu usuario. Tus botones no serán visibles para el resto de usuarios.',
            NOTIFICATION_OPTIONS,
          );
        }
      });
    },
  },
});
