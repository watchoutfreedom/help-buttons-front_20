import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';

export default Controller.extend({
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  actions: {
    backAction() {
      window.history.back();
    },
    closeAction() {
      this.transitionToRoute('index.index');
    },
    searchByAddress(address) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
      };
      return this.transitionToRoute('index', { queryParams });
    },
  },
});
