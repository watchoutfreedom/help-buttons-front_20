import AjaxService from 'ember-ajax/services/ajax';
import { UnauthorizedError } from 'ember-ajax/errors';
import { inject as service } from '@ember/service';

export default AjaxService.extend({
  session: service(),
  namespace: '/api/v1',

  request(url, options) {
    return this._super(url, options)
      .catch((error) => {
        if (error instanceof UnauthorizedError) {
          if (this.get('session.isAuthenticated')) {
            this.session.invalidate();
          }
        }
        throw error;
      });
  },
});
