import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  email: attr('string'),
  name: attr('string'),
  showPhone: attr('boolean', { defaultValue: false }),
  // Attribute when changing password
  password: attr('string'),
  passwordConfirmation: attr('string'),

  avatar: belongsTo('image', { inverse: null }),

  buttons: hasMany('button', { inverse: 'creator' }),

  chats: hasMany('chat', { inverse: null }),
});
