import Component from '@ember/component';
import { BUTTON_TYPES } from '../models/button';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/string';
import { set, get } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['button-new-type'],
  mediaQueries: service(),
  screens: service('screen'),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  nextStep: null,

  BUTTON_TYPES,

  didRenderPage: false,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  isOffer: computed('button.buttonType', function() {
    return get(this, 'button.buttonType') === 'offer';
  }),

  isNeed: computed('button.buttonType', function() {
    return get(this, 'button.buttonType') === 'need';
  }),

  isChange: computed('button.buttonType', function() {
    return get(this, 'button.buttonType') === 'change';
  }),

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-type')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight - 1}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-type')[0];
      const contentHeight = document.getElementsByClassName('button-new-type__body')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ? '' : 'button-new-type__content-has-scroll';
      }
      return '';
    }),

  buttonCardClick: false,

  actions: {
    setButtonType(newType) {
      this.set('button.buttonType', newType);
      this.nextStep();
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
  },
});
