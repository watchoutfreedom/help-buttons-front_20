import Controller from '@ember/controller';

export default Controller.extend({
  button: null,

  actions: {
    nextStep() {
      this.transitionToRoute('buttons.new.description');
    },
  },
});
