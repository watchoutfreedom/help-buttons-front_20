import Controller from '@ember/controller';
import { readOnly } from '@ember/object/computed';

export default Controller.extend({
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),

  socialShareNetwork: null,
  socialShareURL: null,

  actions: {
    closeAction() {
      this.transitionToRoute('index.index.index');
    },
    openConversation() {
      this.transitionToRoute('index.index.button.chat', { queryParams: { id: this.button.id, isButton: true }});
    },
    socialShareTransition() {
      switch (this.socialShareNetwork) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
          break;
        case 'twitter':
          window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
          break;
        case 'linkedin':
          window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
          break;
        case 'whatsapp':
          // window.open('https://wa.me/34618284414/?text=' + this.socialShareURL, '_blank');
          window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
          break;
        default:
      }
    },
  },
});
