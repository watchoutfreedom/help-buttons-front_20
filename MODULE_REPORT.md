## Module Report
### Unknown Global

**Global**: `Ember.Logger`

**Location**: `app/routes/buttons/new/type.js` at line 8

```js
  actions: {
    typeSelected(newType) {
      Ember.Logger.debug('New type selected: ', newType);
      this.transitionTo('buttons.new.description');
    }
```
