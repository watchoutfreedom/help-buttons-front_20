import Mixin from '@ember/object/mixin';
import { computed, set, get, getProperties } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';

export default Mixin.create({
  routing: service('-routing'),
  /**
   * This prop must be overwrited from each controller where this mixin will be imported
   * @type {Array}
   */
  buttons: null,
  buttonsFiltered: null,

  selectedTags: computed(
    'routing.router.currentURL',
    {
      get() {
        const tags = new Array();
        if (!isEmpty(this.routing)) {
          const currentURL = this.routing.router.currentURL;
          if (!isEmpty(currentURL) && currentURL.includes('searchTags')) {
            const tagsIndex = currentURL.indexOf('searchTags');
            let lastTagIndex = currentURL.length;
            let tagUrlParams = currentURL.substring(tagsIndex + 11, lastTagIndex);
            if (tagUrlParams.indexOf('&') !== -1) {
              lastTagIndex = tagUrlParams.indexOf('&');
              tagUrlParams = tagUrlParams.substring(0, lastTagIndex);
            }
            const context = this;
            for (let i = 0; tagUrlParams.includes('%2C'); i++) {
              tags[i] = this.store.query('tag', {
                'filter[name]': tagUrlParams.substring(0, tagUrlParams.indexOf('%2C'))})
                .then((tagsResponse) => {
                  return isEmpty(tagsResponse.firstObject) ? null : tagsResponse.firstObject;
                });
              tagUrlParams = tagUrlParams.replace(tagUrlParams.substring(0, tagUrlParams.indexOf('%2C') + 3), '');
            }

            if (!isEmpty(tagUrlParams)) {
              tags[tags.length] = this.store.query(
                'tag', { 'filter[name]': tagUrlParams.substring(0, tagUrlParams.length) })
                .then((tagsResponse) => {
                  return isEmpty(tagsResponse.firstObject) ? null : tagsResponse.firstObject;
                });
              tagUrlParams = tagUrlParams.replace(tagUrlParams.substring(0, tagUrlParams.length), '');
            }
            Promise.all(tags).then(values => {
              isEmpty(values[0]) ? null : set(this, 'selectedTags', values);
              this.send('updateUrl');
            });
          }
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),


  prevButtonsType: false,
  // TODO: the idea would be to include the others filters in this mixin
  // and this CP will be observing changes in all this filters (buttonsFiltered wouldn't be neccesary so)
  buttonsToShow: computed(
    'buttons.[]',
    'buttonsFiltered.[]',
    'selectedTags.[]',
    'showPulsedButtons',
    function() {
      const { buttons, buttonsFiltered, selectedTags } = getProperties(this,
        'buttons', 'buttonsFiltered', 'selectedTags');

      if (this.prevButtonsType !== this.showPulsedButtons) {
        set(this, 'prevButtonsType', this.showPulsedButtons);
        set(this, 'buttonsFiltered', null);
        set(this, 'resetFilters', true);
      }
      const buttonsToShow = buttonsFiltered === null ? buttons : buttonsFiltered;


      // FILTER BY TAGS
      if(!isEmpty(selectedTags) && !isEmpty(selectedTags[0])) {
        const selectedTagIds = selectedTags.getEach('id').uniq();
        const btnshow = buttonsToShow.filter((button) => {
          const buttonTagIds = [
            ...button.hasMany('neededTags').ids(),
            ...button.hasMany('offerTags').ids(),
          ].uniq();
          const hasTags = selectedTagIds.every((value) => {
            return buttonTagIds.includes(value);
          });
          return hasTags;
        });
        return btnshow;
      }
      return buttonsToShow;
    }
  ),

  lastSearchUrl: null,
  updateUrl: true,
  isFirstLoad: true,

  actions: {
    updateSelectedTags(selectedTags) {
      set(this, 'selectedTags', selectedTags);
      this.send('updateUrl');
    },

    toggleSelectedTag(tag) {
      if(isEmpty(this.selectedTags)) {
        set(this, 'selectedTags', new Array());
        this.selectedTags.pushObject(tag);
      } else {
        if (this.selectedTags.includes(tag)) {
          this.selectedTags.removeObject(tag);
          if (isEmpty(this.selectedTags)) {
            set(this, 'selectedTags', null);
          }
          this.send('updateUrl');
        } else {
          this.selectedTags.pushObject(tag);
          this.send('updateUrl');
        }
      }
    },
    updateUrl() {
      const urlParams = new URLSearchParams(window.location.search);
      let updateUrl = get(this, 'updateUrl');
      let newUrl = '?';

      if (!isEmpty(this.hideWelcome)) {
        newUrl += 'hideWelcome=' + this.hideWelcome + '&'
      }
      if (!isEmpty(this.northEastLat)) {
        newUrl += 'nel=' + this.northEastLat + '&'
      }
      if (!isEmpty(this.northEastLng)) {
        newUrl += 'nelng=' + this.northEastLng + '&'
      }
      if (!isEmpty(this.southWestLat)) {
        newUrl += 'swl=' + this.southWestLat + '&'
      }
      if (!isEmpty(this.southWestLng)) {
        newUrl += 'swlng=' + this.southWestLng + '&'
      }
      if (!isEmpty(this.latitude)) {
        newUrl += 'lat=' + this.latitude + '&'
      }
      if (!isEmpty(this.longitude)) {
        newUrl += 'lng=' + this.longitude + '&'
      }
      if (!isEmpty(this.address)) {
        newUrl += 'adr=' + this.address + '&'
      }
      if (!isEmpty(this.userMarker)) {
        newUrl += 'userMarker=' + this.userMarker + '&'
      }
      if (!isEmpty(this.buttonsDisplay)) {
        newUrl += 'buttonsDisplay=' + this.buttonsDisplay + '&'
      }
      let selectedTagsArray;
      // if (!isEmpty(this.selectedTags) && isEmpty(this.selectedTags[0])) {
      //   set(this, 'selectedTags', this.selectedTags.shift());
      // }
      if (!isEmpty(this.selectedTags) && !isEmpty(this.selectedTags[0])) {
        selectedTagsArray = this.selectedTags.map(function(item) {
          return !isEmpty(item) ? item['name'] : null;
        });
        updateUrl = true;
        if (!isEmpty(this.selectedTags)) {
          newUrl += 'searchTags=' + selectedTagsArray.join('%2C') + '&'
        }
      } else {
        if (!isEmpty(urlParams.get('searchTags')) && get(this, 'isFirstLoad')) {
          newUrl += 'searchTags=' + urlParams.get('searchTags') + '&'
        }
      }
      window.history.pushState({} , 'Help Buttons', newUrl.slice(0, -1));
      set(this, 'isFirstLoad', false);
    },
  },
});
