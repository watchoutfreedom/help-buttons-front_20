import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  belongsToRelationship,
  hasManyRelationship,
} from 'help-button/tests/helpers/model-helpers';

import { run } from '@ember/runloop';

module('Unit | Model | user', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const model = run(() => this.owner.lookup('service:store').createRecord('user'));
    // const store = this.store();
    assert.ok(!!model);
  });

  hasManyRelationship('user', 'buttons', {
    type: 'button',
    options: {
      inverse: 'creator',
    },
  });

  hasManyRelationship('user', 'chats', {
    type: 'chat',
    options: {
      inverse: null,
    },
  });

  belongsToRelationship('user', 'avatar', {
    type: 'image',
    options: {
      inverse: null,
    },
  });
});
