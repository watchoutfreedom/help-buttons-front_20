import { currentURL } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import loginPage from 'help-button/tests/pages/routes/login/index';
import dashboardPage from 'help-button/tests/pages/routes/dashboard/index';

let user;

module('Acceptance | login and logout', function(hooks) {
  setupApplicationTest(hooks);

  hooks.beforeEach(function() {
    user = server.create('user');
    server.create('login', { user });

    server.post('/users/sign_in', (schema) => schema.logins.first());
    server.get('/users/current', () => user);
  });

  function login() {
    loginPage.visit();
    const { loginForm } = loginPage;
    loginForm.email(user.email)
      .password(user.password)
      .submit();
  }

  test('successfully log in', function(assert) {
    login();

    assert.equal(currentURL(), '/dashboard');
  });

  test('successfully log out', function() {
    login();

    dashboardPage.userDashboard.logout();
  });
});
