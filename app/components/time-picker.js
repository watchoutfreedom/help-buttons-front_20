import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  agreedDate: null,
  button: null,
  hours: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  minutes: [ '00', 15, 30, 45],

  time: computed(
    'agreedDate',
    function() {
      const agreedDate = get(this, 'agreedDate');
      const minutes  = agreedDate.getMinutes();
      return agreedDate.getHours() + ':' + (minutes === 0 ? '00' : minutes);
    }
  ),
  isOpened: false,

  timeIndicator: null,
  selectedTimeIndicator: computed(
    'agreedDate',
    function() {
      const agreedDate = get(this, 'agreedDate');
      if (agreedDate !== 'Ahora') {
        if (isEmpty(this.timeIndicator)) {
          if (agreedDate.getHours() >= 12) {
            set(this, 'timeIndicator', 'PM');
            return 'PM';
          }
          set(this, 'timeIndicator', 'AM');
          return 'AM';
        }
        if (agreedDate.getHours() >= 12 && this.timeIndicator === 'PM') {
          return 'PM';
        }
      }
      return 'AM';
    }
  ),


  selectedHour: computed(
    'agreedDate',
    function() {
      const agreedDate = get(this, 'agreedDate');
      if (isEmpty(agreedDate) || agreedDate === 'Ahora') {
        return get(this, 'hours.firstObject');
      }
      let hours = agreedDate.getHours();
      hours = hours % 12;
      hours = hours ? hours : 12;
      return hours;
    }
  ),

  selectedMinutes: computed(
    'agreedDate',
    function() {
      const agreedDate = get(this, 'agreedDate');
      if (isEmpty(agreedDate) || agreedDate === 'Ahora') {
        return get(this, 'minutes.firstObject');
      }
      const minutes = agreedDate.getMinutes();
      return minutes < 10 ? '0' + minutes : minutes;
    }
  ),

  actions: {
    changeTime(propertyName, value, dropdown) {
      if (!isEmpty(value)) {
        set(this, propertyName, value);
      }
      dropdown.actions.close();
      const selectedTimeIndicator = this.selectedTimeIndicator;
      const date = new Date();
      date.setTime(this.agreedDate.getTime());
      switch (propertyName) {
        case 'selectedHour':
          if (selectedTimeIndicator === 'PM' && value !== 12) {
            date.setHours(value + 12);
          } else {
            date.setHours(value);
          }
          if (this.selectedHour === 24) {
            date.setHours('00');
          }
          set(this, 'agreedDate', date);
          break;
        case 'selectedMinutes':
          date.setMinutes(this.selectedMinutes);
          set(this, 'agreedDate', date);
          break;
        case 'selectedTimeIndicator':
          if (selectedTimeIndicator === 'PM') {
            if (this.selectedHour < 12) {
              date.setHours(this.selectedHour + 12);
            }
          } else {
            if (this.selectedHour === 12) {
              date.setHours('00');
            } else {
              date.setHours(this.selectedHour);
            }
          }
          set(this, 'agreedDate', date);
          break;
        default:
          break;
      }
    },
    toggleModal(param) {
      set(this, 'isOpened', param);
    },

    /**
     * @method closeDropdown
     * @return
     */
    closeDropdown(dropdown) {
      dropdown.actions.close();
    },
  },

});
