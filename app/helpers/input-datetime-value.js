import { helper as buildHelper } from '@ember/component/helper';

export function inputDatetimeValue([date]/* , hash*/) {
  if (date && typeof date.toISOString === 'function') {
    return date.toISOString();
  }
  return null;
}

export default buildHelper(inputDatetimeValue);
