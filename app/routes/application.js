import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import { reject } from 'rsvp';

export default Route.extend(ApplicationRouteMixin, {
  setupController(controller, model) {
    this._super(controller, model);
    $('#loader').hide();
  },
  beforeModel: function() {
    return function() {
      if (!localStorage.token) {
        return reject();
      }

      const adapter = this.container.lookup('adapter:application');
      adapter.set('headers', { 'Authorization': localStorage.token });

      return this.get('store').find('user', 'me')
        .then(function(user) {
          return {
            currentUser: user,
          };
        });
    };
  },
});
