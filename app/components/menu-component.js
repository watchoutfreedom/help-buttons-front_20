import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { computed } from '@ember/object';
import { set } from '@ember/object';

export default Component.extend({
  session: service('session'),
  routing: service('-routing'),
  isAuthenticated: readOnly('session.isAuthenticated'),

  isShowingMenuModal: null,
  isSmallDevice: null,
  hideWelcome: null,
  isShowingMoreOptions: false,

  newButtonRoute: computed('routing.currentRouteName', function() {
    if (!this.isSmallDevice) {
      return 'index.index.new-button';
    }
    return 'buttons.new';
  }),

  showSettings: false,

  currentUser: readOnly('session.currentUser'),

  currentUserAvatar: readOnly('currentUser.avatar'),

  isShowingMoreOptions: false,

  actions: {
    toggleProperty(param, hideWelcome) {
      this.toggleProperty(param);
      if (hideWelcome) {
        set(this, 'hideWelcome', false);
      }
    },

    closeSession() {
      this.session.invalidate();
    },
    removeProfile() {
      window.alert('Not finished');
    },

    toggleShowMoreOptions() {
      this.toggleProperty('isShowingMoreOptions');
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index.index') {
          context.loginTransition();
        }
      }, context);
    },
  },
});
