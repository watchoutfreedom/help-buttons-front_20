import Controller from '@ember/controller';
import { get, set, computed } from '@ember/object';
import { readOnly, or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import ButtonsFiltersMixin from '../../mixins/buttons-filters-mixin';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};


export default Controller.extend(
  ButtonsFiltersMixin,
  {
    mediaQueries: service(),
    routing: service('-routing'),

    // QUERY PARAMS
    queryParams: [
      'searchTags',
    ],

    user: readOnly('model'),

    showProfile: false,
    resetFilters: false,
    socialShareNetwork: null,
    socialShareURL: null,

    showPulsedButtons: false,
    buttons: computed(
      'showPulsedButtons',
      'user',
      {
        get() {
          const relationshipName = this.showPulsedButtons ? 'buttons' : 'ownedButtons';
          const user = this.user;

          get(user, relationshipName)
            .then((buttons) => {
              set(this, 'buttons', buttons);
            });
        },
        set(key, value) { return value; },
      }
    ),

    buttonsFiltered: null,

    isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

    selectedButton: null,
    hasSelectedButton: computed('selectedButton', function() {
      if (!isEmpty(this.selectedButton)) {
        document.body.className = 'ember-application overflow-hidden';
        return true;
      }
      document.body.className = 'ember-application';
      return false;
    }),

    actions: {
      searchByAddress(address) {
        const queryParams = {
          latitude: null,
          longitude: null,
          address,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: null,
        };
        return this.transitionToRoute('index', { queryParams });
      },

      setPulsedButtonsFilter(value) {
        set(this, 'showPulsedButtons', value);
      },

      selectButton(selectedButton) {
        set(this, 'selectedButton', selectedButton);
      },

      openConversation() {
        this.transitionToRoute('chats', { queryParams: { id: this.selectedButton.id, isButton: true }});
      },

      closeAction() {
        set(this, 'selectedButton', null);
        set(this, 'showProfile', null);
      },
      goHome() {
        this.send('closeAction');
        this.transitionToRoute('profile.buttons');
      },
      socialShareTransition() {
        switch (this.socialShareNetwork) {
          case 'facebook':
            window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
            break;
          case 'twitter':
            window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
            break;
          case 'linkedin':
            window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
            break;
          case 'whatsapp':
            // window.open('https://wa.me/618284414/?text=' + this.socialShareURL, '_blank');
            window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
            break;
          default:
        }
      },
      toggleProfileState() {
        this.toggleProperty('currentUser.active');
        return this.currentUser.save().then((user) => {
          if (user.active) {
            this.notifications.success(
              'Has activado tu usuario',
              NOTIFICATION_OPTIONS,
            );
          } else {
            this.notifications.info(
              'Has desactivado tu usuario. Tus botones no serán visibles para el resto de usuarios.',
              NOTIFICATION_OPTIONS,
            );
          }
        });
      },
    },
  }
);
