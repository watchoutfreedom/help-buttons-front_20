import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { set, computed, setProperties } from '@ember/object';
import { notEmpty, or, empty } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';

export default Component.extend({
  classNameBindings: ['isShowingMap'],
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  screens: service('screen'),
  isShowingMap: computed('showMap', function() {
    return this.showMap ? 'button-new-location-map' : 'button-new-location';
  }),

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-location')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight - 15}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    'showMap',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-location')[0];
      const contentHeight = document.getElementsByClassName('button-new-location__content')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight) && !this.showMap) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-location__content-has-scroll';
      }
      return '';
    }),

  geolocation: service('geolocation'),

  buttonLocation: computed('button.location.@each', function() {
    if (!isEmpty(this.button.toLatitude) && !isEmpty(this.button.toLongitude)) {
      return 'fromLocationToLocation';
    }
    if (!isEmpty(this.button.latitude) && !isEmpty(this.button.longitude)) {
      if (!isEmpty(this.button.locationName)) {
        return 'mapLocation';
      }
      return 'userLocation';
    }
    return null;
  }),

  button: null,

  showMapButton: computed('buttonLocation', function() {
    const buttonLocation = this.buttonLocation;
    return buttonLocation === 'mapLocation' || buttonLocation === 'fromLocationToLocation';
  }),
  showMap: false,

  autocompletePlaceholder: computed('showSecondMarker', function() {
    return this.showSecondMarker ? 'Escribe auí la dirección de destino' : 'Escribe aquí la dirección de origen';
  }),

  isLocationToLocation: computed(
    'buttonLocation',
    'button.{toLatitude,toLongitude}',
    'showSecondMarker',
    function() {
      if (!this.showSecondMarker || isEmpty(this.button.toLatitude) || isEmpty(this.button.toLongitude)) {
        return this.buttonLocation === 'fromLocationToLocation';
      }
      return false;
    }),

  hasBounds: notEmpty('mapBounds'),

  searchAddressBounds: null,

  mapBounds: computed(
    'geolocation',
    'searchAddressBounds',
    'button.location',
    'showSecondMarker',
    {
      get() {
        if (isEmpty(this.searchAddressBounds)) {
          if (this.showSecondMarker && !isEmpty(this.button.toLatitude) && !isEmpty(this.button.toLongitude)) {
            return L.latLng(this.button.toLatitude, this.button.toLongitude).toBounds(500);
          }
          if (!isEmpty(this.button.latitude) && !isEmpty(this.button.longitude)) {
            return L.latLng(this.button.latitude, this.button.longitude).toBounds(500);
          } else if (!isEmpty(this.button.location.latitude) && !isEmpty(this.button.location.longitude)) {
            return L.latLng(this.button.location.latitude, this.button.location.longitude).toBounds(500);
          }
          this.geolocation
            .getCurrentPosition()
            .then((coordinates) => {
              set(this, 'mapBounds', L.latLng(coordinates.latitude, coordinates.longitude).toBounds(500));
            });
        }
        return this.searchAddressBounds;
      },
      set(key, value) {
        return value;
      },
    }),

  draggableMarkerLocation: computed(
    'mapBounds',
    function() {
      if (!this.showSecondMarker) {
        const mapBounds = this.mapBounds;
        return mapBounds.getCenter();
      }
      return L.latLng(this.button.latitude, this.button.longitude);
    }),

  draggableRedMarkerLocation: computed(
    'mapBounds',
    'showSecondMarker',
    function() {
      if (this.showSecondMarker) {
        // Setting toLat & toLong on 1 marker;
        if (isEmpty(this.button.toLatitude) || isEmpty(this.button.toLongitude)) {
          const mapBounds = this.mapBounds;
          return mapBounds.getCenter();
        }
        return L.latLng(this.button.toLatitude, this.button.toLongitude);
      }
      return null;
    }),

  loadedMap: false,
  buttonCardClick: false,
  inputAddress: null,

  nextStepAction() {},

  isLoading: false,

  searchZoom: false,
  isSearchingAddress: false,
  showSecondMarker: false,

  isNextButtonDisabled: empty('buttonLocation'),

  actions: {
    setRadioButton(value) {
      set(this, 'buttonLocation', value);
    },

    placeChangedCallback(params) {
      if (!isEmpty(params.geometry)) {
        const viewport = params.geometry.viewport;
        const northEastCorner = L.latLng(viewport.getNorthEast().lat(), viewport.getNorthEast().lng());
        const southWestCorner = L.latLng(viewport.getSouthWest().lat(), viewport.getSouthWest().lng());
        setProperties(this, {
          'searchAddressBounds': L.latLngBounds(northEastCorner, southWestCorner),
          'searchZoom': true,
          'isSearchingAddress': true,
        });
        if (!this.showSecondMarker) {
          setProperties(this, {
            'button.latitude': params.geometry.location.lat(),
            'button.longitude': params.geometry.location.lng(),
            'button.locationName': params.formatted_address,
          });
        } else {
          setProperties(this, {
            'button.toLatitude': params.geometry.location.lat(),
            'button.toLongitude': params.geometry.location.lng(),
            'button.toLocationName': params.formatted_address,
          });
        }
      } else {
        this.send('searchByAddress');
      }
    },
    onDragEnd(marker) {
      setProperties(this, {
        'button.latitude': marker.target._latlng.lat,
        'button.longitude': marker.target._latlng.lng,
      });
    },

    onDragEndSecondMarker(marker) {
      setProperties(this, {
        'button.toLatitude': marker.target._latlng.lat,
        'button.toLongitude': marker.target._latlng.lng,
      });
    },

    nextMarker() {
      setProperties(this, {
        'inputAddress': null,
        'showSecondMarker': true,
      });
      if (isEmpty(this.button.toLatitude) || isEmpty(this.button.toLongitude)) {
        const mapCenter = this.mapBounds.getCenter();
        setProperties(this, {
          'button.toLatitude': mapCenter.lat,
          'button.toLongitude': mapCenter.lng,
        });
      } else {
        setProperties(this, {
          'button.toLatitude': this.button.toLatitude,
          'button.toLongitude': this.button.toLongitude,
        });
      }
    },
    nextStep() {
      set(this, 'isLoading', true);
      const buttonLocation = this.buttonLocation;
      if (buttonLocation === 'userLocation') {
        setProperties(this, {
          'button.latitude': null,
          'button.longitude': null,
          'button.toLatitude': null,
          'button.toLongitude': null,
        });
        // Geolocate user
        this.geolocation
          .getCurrentPosition()
          .then((coordinates) => {
            const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            coordinates.latitude + ',' + coordinates.longitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
            $.getJSON(geocodingAPI).then((json) => {
              if (json.status === 'OK') {
                const result = json.results[0];
                let state = '';
                let locationName = result.formatted_address.split(',');
                locationName = locationName.reverse();
                locationName = locationName[2] + ',' + locationName[1] + ',' + locationName[0];
                locationName = locationName.substr(locationName.substr(1).indexOf(' ') + 2, locationName.length);
                setProperties(this, {
                  'button.latitude': coordinates.latitude,
                  'button.longitude': coordinates.longitude,
                  'button.toLatitude': null,
                  'button.toLongitude': null,
                  'button.locationName': locationName,
                });
                this.nextStepAction();
                // set(this, 'showSpinner', false);
              }
            });
          });
      } else {
        if (this.buttonLocation === 'mapLocation') {
          setProperties(this, {
            'button.toLatitude': null,
            'button.toLongitude': null,
          });
        }
        if (isEmpty(this.button.latitude) || isEmpty(this.button.longitude)) {
          if (this.buttonLocation === 'fromLocationToLocation') {
            setProperties(this, {
              'button.latitude': null,
              'button.longitude': null,
            });
          } else {
            setProperties(this, {
              'button.latitude': null,
              'button.longitude': null,
              'button.toLatitude': null,
              'button.toLongitude': null,
            });
          }
          setProperties(this, {
            'button.latitude': this.draggableMarkerLocation.lat,
            'button.longitude': this.draggableMarkerLocation.lng,
            'button.toLatitude': null,
            'button.toLongitude': null,
          });
        }
        if (this.buttonLocation === 'fromLocationToLocation' &&
        (isEmpty(this.button.toLatitude) || isEmpty(this.button.toLongitude))) {
          setProperties(this, {
            'button.toLatitude': this.draggableMarkerLocation.lat,
            'button.toLongitude': this.draggableMarkerLocation.lng,
          });
        }
        // Add location names
        if (this.buttonLocation === 'mapLocation') {
          const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
          this.button.latitude + ',' + this.button.longitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
          $.getJSON(geocodingAPI).then((json) => {
            if (json.status === 'OK') {
              const result = json.results[0];
              let state = '';
              let locationName = result.formatted_address.split(',');
              locationName = locationName.reverse();
              locationName = locationName[2] + ',' + locationName[1] + ',' + locationName[0];
              locationName = locationName.substr(locationName.substr(1).indexOf(' ') + 2, locationName.length);
              setProperties(this, {
                'button.toLatitude': null,
                'button.toLongitude': null,
                'button.locationName': locationName,
              });
            }
          });
        } else if (this.buttonLocation === 'fromLocationToLocation') {
          if (isEmpty(this.button.locationName)) {
            const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            this.button.latitude + ',' + this.button.longitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
            $.getJSON(geocodingAPI).then((json) => {
              if (json.status === 'OK') {
                const result = json.results[0];
                let state = '';
                let locationName = result.formatted_address.split(',');
                locationName = locationName.reverse();
                locationName = locationName[2] + ',' + locationName[1] + ',' + locationName[0];
                locationName = locationName.substr(locationName.substr(1).indexOf(' ') + 2, locationName.length);
                setProperties(this, {
                  'button.locationName': locationName,
                });
              }
            });
          }
          if (isEmpty(this.button.toLocationName)) {
            const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            this.button.toLatitude + ',' + this.button.toLongitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
            $.getJSON(geocodingAPI).then((json) => {
              if (json.status === 'OK') {
                const result = json.results[0];
                let state = '';
                let locationName = result.formatted_address.split(',');
                locationName = locationName.reverse();
                locationName = locationName[2] + ',' + locationName[1] + ',' + locationName[0];
                locationName = locationName.substr(locationName.substr(1).indexOf(' ') + 2, locationName.length);
                setProperties(this, {
                  'button.toLocationName': locationName,
                });
              }
            });
          }
        }
        this.nextStepAction();
      }
    },
    cancelMarkerLocation() {
      set(this, 'showMap', false);
    },
    removeSecondMarker() {
      const loading = this.isLoading;
      setProperties(this, {
        'showSecondMarker': false,
        'isLoading': false,
      });
      if (loading) {
        this.nextStepAction(true);
      }
    },
    onMapLoad() {
      const center = this.mapBounds.getCenter();
      setProperties(this, {
        'button.latitude': center.lat,
        'button.longitude': center.lng,
      });
    },
    toggleProperty(param) {
      this.toggleProperty(param);
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
  },
});
