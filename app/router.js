import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
});

Router.map(function() {
  this.route('login');
  this.route('dashboard');
  this.route('signup');

  this.route('buttons', function() {
    this.route('new', { path: '/:button_id'}, function() {
      this.route('description');
      this.route('location');
      this.route('activate-button');
      this.route('share');
      this.route('login');
      this.route('register');
      this.route('date');
    });
  });

  this.route('profile', function() {
    this.route('buttons');
  });
  this.route('components-repo');
  this.route('register');
  this.route('recovery');
  this.route('index', { path: '' }, function() {
    this.route('index', { path: '' }, function() {
      this.route('button', function() {
        this.route('index', { path: '/:button_id'});
        this.route('chat');
      });
      this.route('new-button', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('login');
        this.route('register');
        this.route('share');
        this.route('date');
      });

      this.route('new', function() {});
    });
  });
  this.route('user', { path: 'user/:user_id'}, function() {
    this.route('buttons');
  });
  this.route('profile-edition');
  this.route('others');
});

export default Router;
