import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { equal, notEmpty, or } from '@ember/object/computed';
import { inject as service } from '@ember/service';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Component.extend({
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  notifications: service('notification-messages'),
  showEdit: computed(
    'allowEdit',
    'button.isMine',
    function() {
      return this.allowEdit && this.button.isMine;
    }
  ),

  allowEdit: null,
  button: null,
  creator: null,
  creatorAvatar: null,
  isList: null,
  isButtonFile: null,
  isActivateRoute: null,

  allowMoveMap: computed('button', function() {
    return !isEmpty(this.button.toLatitude) || !isEmpty(this.button.toLatitude);
  }),

  hasLocationName: notEmpty('button.locationName'),

  hasFromToLocation: computed(
    'button.latitude',
    'button.longitude',
    'button.toLatitude',
    'button.toLongitude',
    function() {
      return !isEmpty(this.button.latitude) && !isEmpty(this.button.longitude) &&
        !isEmpty(this.button.toLatitude) && !isEmpty(this.button.toLongitude);
    }),

  mapBounds: computed('firstMarker', 'secondMarker', function() {
    return L.latLngBounds(get(this, 'firstMarker'), get(this, 'secondMarker'));
  }),

  firstMarker: computed('button', function() {
    return L.latLng(this.button.latitude, this.button.longitude);
  }),

  secondMarker: computed('button', function() {
    return L.latLng(this.button.toLatitude, this.button.toLongitude);
  }),

  formatedDate: computed('button.date', function() {
    let date = get(this, 'button.date');
    const options = {
      weekday: 'long', month: 'long',
      day: 'numeric', hour: '2-digit', minute: '2-digit',
    };
    if (!isEmpty(date) && date !== 'Ahora') {
      date = new Date(date);
      date = date.toLocaleTimeString('es-es', options);
      return date.charAt(0).toUpperCase() + date.slice(1);
    }
    return 'Ahora';
  }),

  hasFormatedDate: notEmpty('formatedDate'),

  hasCreatorAvatar: notEmpty('creatorAvatar.content'),

  hasImage: notEmpty('image'),

  image: computed(
    'button',
    {
      get() {
        const button = this.button;
        get(button, 'image').then((image) => {
          set(this, 'image', image);
        });
      },
      set(key, value) {
        return value;
      },
    }
  ),

  cardBtnType: computed(
    'button',
    function() {
      if (!this.button.active && !this.isButtonFile && !this.isActivateRoute) {
        if (get(this, 'button.isChangeButton')) {
          if (this.button.swap) {
            return '--intercambio-swap  card-btn--intercambio-swap-deactivated';
          }
          return '--intercambio card-btn--intercambio-deactivated';
        }
        if (get(this, 'button.isOfferButton')) {
          return '--green card-btn--green-deactivated';
        }
        if (get(this, 'button.isNeedButton')) {
          return '--red card-btn--red-deactivated';
        }
        return null;
      }
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--green';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--red';
      }
      return null;
    }
  ),

  cardStateBtnType: computed(
    'button',
    function() {
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--ofrece';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--busca';
      }
      return null;
    }
  ),

  buttonType: computed(
    'button',
    'button.active',
    function() {
      if (!this.button.active && !this.isButtonFile && !this.isActivateRoute) {
        if (get(this, 'button.isChangeButton')) {
          if (this.button.swap) {
            return '--intercambio-swap  card-button--intercambio-swap-deactivated';
          }
          return '--intercambio card-button--intercambio-deactivated';
        }
        if (get(this, 'button.isOfferButton')) {
          return '--ofrece card-button--ofrece-deactivated';
        }
        if (get(this, 'button.isNeedButton')) {
          return '--busca card-button--busca-deactivated';
        }
        return null;
      }
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--ofrece';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--busca';
      }
      return null;
    }
  ),

  buttonIntention: computed('buttonType', function() {
    const buttonType = this.buttonType;
    switch (buttonType) {
      case '--intercambio':
        return 'OFRECE y BUSCA A CAMBIO';
      case '--intercambio-swap':
        return 'BUSCA y OFRECE A CAMBIO';
      case '--ofrece':
        return 'OFRECE';
      case '--busca':
        return 'BUSCA / NECESITA';
      case '--intercambio card-button--intercambio-deactivated':
        return 'OFRECE y BUSCA A CAMBIO';
      case '--ofrece card-button--ofrece-deactivated':
        return 'OFRECE';
      case '--busca card-button--busca-deactivated':
        return 'BUSCA / NECESITA';
      case '--intercambio-swap  card-button--intercambio-swap-deactivated':
        return 'BUSCA y OFRECE A CAMBIO';
      default:
        return 'OFRECE y BUSCA A CAMBIO';
    }
  }),

  needsAndOffer: equal('buttonIntention', 'OFRECE y BUSCA A CAMBIO'),

  profileRoute: computed(
    'button.isMine',
    function() {
      return get(this, 'button.isMine') ? 'profile.buttons' : 'user.buttons';
    }
  ),

  linkSegmentId: computed(
    'button.isMine',
    function() {
      const isMine = get(this, 'button.isMine');
      return isMine ? null : this.button.belongsTo('creator').id();
    }
  ),

  showProfile: null,
  routeType: null,
  removeButtonTransition: null,
  showMap: false,

  actions: {
    toggleAttr(param) {
      this.toggleProperty(param);
    },
    removeButton() {
      this.button.deleteRecord();
      this.button.save()
        .then(() => {
          this.notifications.success(
            'Botón borrado correctamente',
            NOTIFICATION_OPTIONS,
          );
          this.removeButtonTransition();
        })
        .catch(() => {
          this.notifications.error(
            'Error al borrar botón',
            NOTIFICATION_OPTIONS,
          );
        });
    },
    onMapLoad(leaflet) {
      L.Routing.control({
        waypoints: [
            L.latLng(this.button.latitude, this.button.longitude),
            L.latLng(this.button.toLatitude, this.button.toLongitude)
        ],
        createMarker: function(i, waypoints, n){
          return L.marker(L.latLng(0, 0), {
					draggable: false,
          })
	      },
        routeWhileDragging: false,
        addWaypoints: false,
        altLineOptions: true
      }).addTo(leaflet.target);
    }
  },
});
