import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';

export default Controller.extend({
  button: alias('model'),

  actions: {
    toggleButtonState() {
      this.toggleProperty('button.active');
      this.button.save()
        .then(() => {
          this.transitionToRoute('buttons.new.share');
        });
    },
  },
});
