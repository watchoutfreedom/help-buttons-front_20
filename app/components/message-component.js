import Component from '@ember/component';
import { get, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { notEmpty } from '@ember/object/computed';

export default Component.extend({
  classNames: ['d-flex'],
  avatar: null,
  message: null,
  otherUser: null,
  currentUser: null,

  hasAvatar: notEmpty('avatar'),

  isAuthor: computed('message', 'otherUser', 'currentUser', function() {
    if (isEmpty(get(this, 'message.user.content.id'))) {
      return true;
    }
    return get(this, 'currentUser.id') === get(this, 'message.user.content.id');
  }),

  time: computed('message.createdAt', function() {
    const time = get(this, 'message.createdAt');
    if (!isEmpty(time)) {
      const minutes = time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes();
      return time.getHours() + ':' + minutes;
    }
    return null;
  }),


  didRender() {
    this.send('scrollBottom');
  },

  actions: {
    scrollBottom() {
      const chatConversation = document.getElementById('chatConversation');
      if(!isEmpty((chatConversation))) {
        chatConversation.scrollTop = chatConversation.scrollHeight;
      }
    },
  },
});
