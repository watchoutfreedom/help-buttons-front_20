import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { resolve } from 'rsvp';

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),

    model() {
      const currentUser = this.session.currentUser;
      return currentUser.constructor.toString() === 'DS.PromiseObject' ?
        currentUser : resolve(currentUser);
    },
  }
);
