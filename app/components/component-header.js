import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { or, readOnly, alias } from '@ember/object/computed';
import { computed, set, get, setProperties } from '@ember/object';
import { A } from '@ember/array';
import { isEmpty } from '@ember/utils';


export default Component.extend({
  classNames: ['component-header', 'd-block'],
  classNameBindings: ['useContainer'],
  store: service(),

  session: service('session'),
  ajax: service(),
  mediaQueries: service(),
  routing: service('-routing'),

  isAuthenticated: readOnly('session.isAuthenticated'),

  currentUser: alias('session.currentUser'),

  currentUserAvatar: readOnly('session.currentUser.avatar'),

  modalBackground: computed('isAuthenticated', function() {
    if (this.isAuthenticated) {
      return 'component-header__container-modal';
    }
    return 'p-2 bg-primary h-100 w-100';
  }),

  useContainer: computed('isSmallDevice', function() {
    if (this.isSmallDevice) {
      return '';
    }
    return 'container-fluid';
  }),

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  showLinkToHome: null,
  showBackBtn: null,

  isMyButtonsRoute: null,

  isShowingModal: false,
  isShowingMenuModal: false,

  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  searchAction: null,
  inputAddress: null,

  hideWelcome: null,

  loginTransition: null,

  selectedFilterTags: A([]),

  toggleSelectedTagAction() {},

  actions: {
    searchByAddress() {
      set(this, 'isSearching', true);
      const address = this.inputAddress;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
          });
        });
    },

    toggleModal(param) {
      this.toggleProperty(param);
    },
    closedToggleSelectedTag([ tag ]) {
      if (this.routing.currentRouteName.includes('index.index') ||
      (this.routing.currentRouteName.includes('profile') &&
      !this.routing.currentRouteName.includes('profile-edition'))) {
        this.toggleSelectedTagAction(tag);
      } else {
        set(this, 'isSearching', true);
        const address = this.inputAddress;
        return this.searchAction(address, tag.name)
          .then(() => {
            setProperties(this, {
              'isSearching': false,
              'inputAddress': null,
            });
          });
      }
    },
    searchTags(term, selectClass) {
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            if (!isEmpty(tags.get('firstObject'))) {
              return this.toggleSelectedTagAction(tags.get('firstObject'));
            }
            return null;
          });
      }
      return this.store.query('tag', { 'filter[name]': term })
        .then((tags) => {
          return tags.filter((tag) => !this.selectedFilterTags.includes(tag));
        });
    },

    tagSuggestion() {
      return '';
    },

    closeDropdown(dropdown) {
      if (!this.routing.currentRouteName.includes('index.index.index')
      && !this.routing.currentRouteName.includes('profile.buttons')
      && !this.routing.currentRouteName.includes('user.buttons')
      && !this.routing.currentRouteName.includes('others')
      && !this.routing.currentRouteName.includes('profile-edition')) {
        dropdown.actions.close();
      }
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index.index') {
          context.loginTransition();
        }
      }, context);
    },
  },
});
