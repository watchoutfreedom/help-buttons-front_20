import Controller from '@ember/controller';
import DateStepMixin from '../../../mixins/date-step-mixin';

export default Controller.extend(
  DateStepMixin,
  {
    activationRoute: 'buttons.new.activate-button',
  }
);
