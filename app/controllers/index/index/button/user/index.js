import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';

export default Controller.extend({
  user: alias('model.user'),
  avatar: alias('model.avatar'),

  actions: {
    goBack() {
      window.history.back();
    },
  },
});
