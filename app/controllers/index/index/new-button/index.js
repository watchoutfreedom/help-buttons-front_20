import Controller from '@ember/controller';

export default Controller.extend({
  button: null,

  actions: {
    nextStep() {
      this.transitionToRoute('index.index.new-button.description');
    },
    closeAction() {
      this.transitionToRoute('index.index');
    },
  },
});
