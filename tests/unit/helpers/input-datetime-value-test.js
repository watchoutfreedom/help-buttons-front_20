import { inputDatetimeValue } from 'help-button/helpers/input-datetime-value';
import { module, test } from 'qunit';

module('Unit | Helper | input datetime value', function() {
  // Replace this with your real tests.
  test('it works', function(assert) {
    assert.equal(typeof inputDatetimeValue, 'function');
  });
});
