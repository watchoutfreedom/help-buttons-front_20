import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { empty, or } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';

export default Component.extend({
  classNames: ['button-new-date'],
  geolocation: service('geolocation'),
  screens: service('screen'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-date')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-date')[0];
      const contentHeight = document.getElementsByClassName('button-new-date__body')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-date__content-has-scroll';
      }
      return '';
    }),

  button: null,

  buttonDateType: computed('button.date', function() {
    if (this.button.date === 'Ahora') {
      return 'now';
    }
    return !isEmpty(this.button.date) ? 'specificDate' : null;
  }),

  showPicker: null,

  selected: computed('button.date', function() {
    return !isEmpty(this.button.date) ? this.button.date : new Date();
  }),

  bodyClass: computed('isSmallDevice', function() {
    return this.isSmallDevice ?
      'button-new-date__body' : 'button-new-date__body button-new-date__body--small-height';
  }),

  inputAddress: null,

  date: computed('button.date', function() {
    return !isEmpty(this.button.date) ? this.button.date : new Date();
  }),

  isNextButtonDisabled: empty('buttonDateType'),

  nextStepAction() {},

  buttonCardClick: false,

  actions: {
    nextStep() {
      if (this.buttonDateType === 'now') {
        set(this, 'button.date', 'Ahora');
      } else if (!isEmpty(this.selected)) {
        set(this, 'button.date', this.selected);
      }
      this.nextStepAction();
    },

    closeModal() {
      set(this, 'showPicker', false);
    },

    togglePicker(param) {
      set(this, 'button.date', null);
      this.toggleProperty('showPicker');
      set(this, 'buttonDateType', param);
    },

    confirmDate() {
      const date = this.selected;
      date.setHours(this.date.getHours());
      date.setMinutes(this.date.getMinutes());
      set(this, 'button.date', date);
      this.send('closeModal');
      this.send('nextStep');
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    closeCreateModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
    setRadioButton(value) {
      set(this, 'buttonDateType', value);
    },
  },
});
