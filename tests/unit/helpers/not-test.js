import { not } from 'help-button/helpers/not';
import { module, test } from 'qunit';

module('Unit | Helper | not', function() {
  // Replace this with your real tests.
  test('it returns the negation ofthe first param', function(assert) {
    const truthyValues = [
      'hola',
      1,
      true,
      { key: 'something' },
      {},
      [1, 2, 3],
      [],
    ];
    const falsyValues = [
      '',
      0,
      false,
    ];

    truthyValues.forEach((value) =>
      assert.notOk(not([value]), `wrong result for ${JSON.stringify(value)} (expected false)`));
    falsyValues.forEach((value) =>
      assert.ok(not([value]), `wrong result for ${JSON.stringify(value)} (expected true)`));
  });
});
