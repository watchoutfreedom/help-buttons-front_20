import Component from '@ember/component';
import { computed } from '@ember/object';
import { set } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['button-new-activate'],
  screens: service('screen'),

  toggleButtonState: null,

  buttonClass: computed('button.active', function() {
    return !this.button.active ?
      'buttons-new-activate-button__inactive-button button-file__yellow-circle' +
      ' button-file__yellow-circle--big-circle button-file__yellow-circle-content' :
      'button-file__yellow-circle' +
      ' button-file__yellow-circle--big-circle button-file__yellow-circle-content';
  }),


  nextStep: null,

  didRenderPage: false,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-activate')[0];
      const contentHeight = document.getElementsByClassName('button-new-activate__content')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-activate__content-has-scroll';
      }
      return '';
    }),


  buttonCardClick: false,

  actions: {
    toggleButtonState() {
      this.toggleButtonState();
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
  },
});
