import Model from 'ember-data/model';
import ModelMixin from 'ember-data-updating-json-api-relationships/mixins/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { inject as service } from '@ember/service';
import { computed, get, set, getProperties } from '@ember/object';
import { readOnly, equal } from '@ember/object/computed';
import { resolve } from 'rsvp';
import { isEmpty } from '@ember/utils';

export const NEED = 'need';
export const OFFER = 'offer';
export const CHANGE = 'change';

export const BUTTON_TYPES = { NEED, OFFER, CHANGE };

export default Model.extend(
  ModelMixin,
  {
    //  SERVICES
    session: service('session'),

    // MODEL ATTRIBUTES
    description: attr('string'),
    fullAddress: attr('string'),
    buttonType: attr('string'),
    swap: attr('boolean'),
    active: attr('boolean'),
    latitude: attr('number'),
    longitude: attr('number'),
    actionEvent: attr(),
    chatsCounter: attr('number'),
    sharedCounter: attr('number'),
    date: attr('string'),
    createdAt: attr('date'),
    toLatitude: attr('number'),
    toLongitude: attr('number'),
    locationName: attr('string'),
    toLocationName: attr('string'),


    // MODEL RELATIONSHIPS
    creator: belongsTo('user', { inverse: null }),
    image: belongsTo('image', { inverse: null }),
    offerTags: hasMany('tag', { inverse: null }),
    neededTags: hasMany('tag', { inverse: null }),
    chats: hasMany('chat'),

    // COMPUTED PROPERTIES
    isAuthenticated: readOnly('session.isAuthenticated'),
    currentUser: readOnly('session.currentUser'),

    isNeedButton: equal('buttonType', NEED),
    isOfferButton: equal('buttonType', OFFER),
    isChangeButton: equal('buttonType', CHANGE),

    isMine: computed(
      'isAuthenticated',
      'currentUser.internalId',
      'creator.id',
      {
        get() {
          if(get(this, 'isAuthenticated')) {
            if (!isEmpty(this.currentUser)) {
              const currentUserPromise = get(this, 'currentUser').constructor.toString() === 'DS.PromiseObject' ?
                get(this, 'currentUser') : resolve(get(this, 'currentUser'));
              currentUserPromise.then((currentUser) => {
                const isMine = currentUser.internalId.toString() === this.belongsTo('creator').id();
                set(this, 'isMine', isMine);
              });
            }
          }
          return false;
        },
        set(key, value) { return value; },
      }
    ),

    location: computed(
      'latitude',
      'longitude',
      function() {
        const { latitude, longitude } = getProperties(this, 'latitude', 'longitude');
        return [ latitude, longitude ];
      }
    ),

    hasValidTags: computed(
      'buttonType',
      'offerTags',
      'neededTags',
      function() {
        switch (get(this, 'buttonType')) {
          case NEED:
            return this.hasMany('neededTags').ids().length > 0;
          case OFFER:
            return this.hasMany('offerTags').ids().length > 0;
          case CHANGE:
            return this.hasMany('neededTags').ids().length > 0 &&
              this.hasMany('offerTags').ids().length > 0;
          default:
            return false;
        }
      }
    ),
  }
);
