import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import { set } from '@ember/object';
import { hash } from 'rsvp';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 4000,
};

export const POLL_INTERVAL = 10000;

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    authenticationRoute: 'register',

    notifications: service('notification-messages'),

    queryParams: {
      isButton: {
        refreshModel: true,
        as: 'isButton',
      },
      id: {
        refreshModel: true,
        as: 'id',
      },
    },

    _findOrCreateChat(button) {
      if (isEmpty(button)) {
        this.notifications.error(
          'No se ha encontrado el chat',
          NOTIFICATION_OPTIONS,
        );
      }
      return button.chats.then((chat) => {
        if (isEmpty(chat.firstObject)) {
          return this.store.createRecord('chat', {
            button: button,
          }).save()
            .then((newChat) => {
              return hash({
                chat: newChat,
                button,
                messages: newChat.messages,
              });
            });
        }
        return hash({
          chat: chat.firstObject,
          button: button,
          messages: chat.firstObject.messages,
        });
      });
    },
    _findChatButton(chat) {
      if (isEmpty(chat)) {
        this.notifications.error(
          'No se ha encontrado el chat',
          NOTIFICATION_OPTIONS,
        );
      }
      return chat.button.then((button) => {
        return hash({
          chat: this.chat,
          button: button,
          messages: this.chat.messages,
        });
      });
    },

    model(params) {
      if (params.isButton === 'true') {
        const button = this.store.peekRecord('button', params.id);
        if (isEmpty(button)) {
          return this.store.findRecord('button', params.id).then((buttonResponse) => {
            set(this, 'button', buttonResponse);
            return this._findOrCreateChat(buttonResponse);
          })
            .catch(()=>{
              this.notifications.error('No se ha encontrado el chat porque el boton no existe', NOTIFICATION_OPTIONS);
              this.transitionTo('index.index');
            });
        }
        return this._findOrCreateChat(button);
      }
      const chat = this.store.peekRecord('chat', params.id);
      if (isEmpty(chat)) {
        return this.store.findRecord('chat', params.id).then((chatResponse) => {
          set(this, 'chat', chatResponse);
          return this._findChatButton(chatResponse);
        })
          .catch(()=>{
            this.notifications.error('No se ha encontrado el chat', NOTIFICATION_OPTIONS);
            this.transitionTo('index.index');
          });
      }
      set(this, 'chat', chat);
      return this._findChatButton(chat);
    },

    afterModel(model) {
      if (!isEmpty(model.chat)) {
        let poller = this.chatPoller;

        if (poller) {
          this.pollboy.remove(poller);
        }

        poller = this.pollboy.add(this, () => model.messages.reload(), POLL_INTERVAL);

        this.set('chatPoller', poller);
        return model.messages.reload();
      }
      return null;
    },

    deactivate() {
      const poller = this.chatPoller;

      if (poller) {
        this.pollboy.remove(poller);
      }
    },
  });
