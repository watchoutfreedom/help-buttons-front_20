import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    nextStep() {
      this.transitionToRoute('index.index.new-button.date');
    },
    closeAction() {
      this.transitionToRoute('index.index');
    },
  },
});
