import Controller from '@ember/controller';
import { readOnly } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  routing: service('-routing'),
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),

  buttonFileClass: computed('routing.currentPath', function() {
    if (this.routing.currentPath === 'index.index.button.chat') {
      return 'button-file__modal button-file__modal--top-position';
    }
    return 'button-file__modal';
  }),
  actions: {
    closeAction() {
      this.transitionToRoute('index.index.index');
    },
    openConversation() {
      this.transitionToRoute('chats', { queryParams: { id: this.button.id, isButton: true }});
    },
  },
});
