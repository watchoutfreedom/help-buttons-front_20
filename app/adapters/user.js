import ApplicationAdapter from './application';
import { isEmpty } from '@ember/utils';
import { set } from '@ember/object';

export default ApplicationAdapter.extend({
  urlForUpdateRecord(id, modelName, snapshot) {
    const user = snapshot.record;
    if (!isEmpty(user.currentPassword) &&
    !isEmpty(user.password) &&
    !isEmpty(user.passwordConfirmation) &&
    user.actionEvent === 'updatePassword') {
      set(user, 'actionEvent', null);
      return `/api/v1/users/${user.internalId}/password`;
    }
    if (!isEmpty(snapshot.adapterOptions) && !isEmpty(snapshot.adapterOptions.relationshipToUpdate)) {
      return `/api/v1/users/${user.id}/relationships/` + snapshot.adapterOptions.relationshipToUpdate;
    }
    return `/api/v1/users/${user.id}`;
  },
});
