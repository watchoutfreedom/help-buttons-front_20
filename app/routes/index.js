import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { hash } from 'rsvp';
import currentUser from 'help-button/utils/current-user';
import { readOnly } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Route.extend(
  {
    geolocation: service('geolocation'),
    notifications: service('notification-messages'),
    session: service('session'),
    isAuthenticated: readOnly('session.isAuthenticated'),
    currentUser: currentUser('session'),

    queryParams: {
      latitude: {
        refreshModel: true,
        as: 'lat',
      },
      longitude: {
        refreshModel: true,
        as: 'lng',
      },
      northEastLat: {
        refreshModel: true,
        as: 'nel',
      },
      northEastLng: {
        refreshModel: true,
        as: 'nelng',
      },
      southWestLat: {
        refreshModel: true,
        as: 'swl',
      },
      southWestLng: {
        refreshModel: true,
        as: 'swlng',
      },
      address: {
        refreshModel: true,
        as: 'adr',
      },
      userMarker: {
        refreshModel: false,
        as: 'user',
      },
      hideWelcome: {
        refreshModel: true,
        as: 'hideWelcome',
      },
      buttonsDisplay: {
        refreshModel: true,
        as: 'buttonsDisplay',
      },
      searchTags: {
        refreshModel: true,
        as: 'searchTags',
      },
    },

    hideWelcome: false,

    lastResults: null,

    urlTargetName: null,

    /**
     * Checks if passed query params have all neccesary properties
     * to perform anyone search properly
   * @method _checkQueryParams
   * @param  {Object}          queryParams
   * @return {Boolean}
   */
    _checkQueryParams(queryParams) {
      const hasPointCoords = !isEmpty(queryParams) &&
        !isEmpty(queryParams.lat) &&
        !isEmpty(queryParams.lng);

      const hasBoundCoords = !isEmpty(queryParams.nel) &&
        !isEmpty(queryParams.nelng) &&
        !isEmpty(queryParams.swl) &&
        !isEmpty(queryParams.swlng);

      const hasAddressParam = !isEmpty(queryParams.adr);

      return hasPointCoords || hasBoundCoords || hasAddressParam;
    },

    beforeModel(transition) {
      set(this, 'urlTargetName', transition.targetName);
      const indexController = this.controllerFor('index/index');
      /*
       * If map is loaded, save current transition and abort the previous
       * (see '_replaceTransition' method in index controller)
       */
      indexController._replaceTransition(transition);
      // Add check if force close
      if (this.isAuthenticated) {
        set(this, 'hideWelcome', true);
      }
      /*
     * if the QP aren't enought to perform a search its made
     * an user geolocation then we make a transition to this same route again
     * but with the geolocation coordinates as query params
     */
      if(!this._checkQueryParams(transition.queryParams)) {
        return this.geolocation
          .getCurrentPosition()
          .then((coordinates) => {
            const queryParams = {
              latitude: coordinates.latitude,
              longitude: coordinates.longitude,
              northEastLat: null,
              northEastLng: null,
              southWestLat: null,
              southWestLng: null,
              hideWelcome: this.hideWelcome,
            };
            this.transitionTo(this.urlTargetName, { queryParams });
          });
      }
      return null;
    },

    model(params) {
      const queryParams = {};
      const {
        latitude,
        longitude,
        northEastLat,
        northEastLng,
        southWestLat,
        southWestLng,
        address,
        hideWelcome,
        buttonsDisplay,
      } = params;

      /*
       * We check each QP to send on search the QP with value only
       */
      if(!isEmpty(latitude)) { queryParams.lat = latitude; }
      if(!isEmpty(longitude)) { queryParams.lng = longitude; }
      if(!isEmpty(northEastLat)) { queryParams.nel = northEastLat; }
      if(!isEmpty(northEastLng)) { queryParams.nelng = northEastLng; }
      if(!isEmpty(southWestLat)) { queryParams.swl = southWestLat; }
      if(!isEmpty(southWestLng)) { queryParams.swlng = southWestLng; }
      if(!isEmpty(address)) { queryParams.adr = address; }
      if(!isEmpty(this.hideWelcome)) { queryParams.hideWelcome = hideWelcome; }
      if(!isEmpty(buttonsDisplay)) { queryParams.buttonsDisplay = buttonsDisplay; }

      // Buttons search
      const buttons = this.store.query('button', queryParams)
        .then((buttonsResponse) => {
          // Check if buttons response has empty Meta
          const emptyMeta = Object.keys(buttonsResponse.meta).length === 0 &&
          typeof buttonsResponse.meta === 'object';
          // Check if is meta is empty & the query param address is passed
          if(emptyMeta && !isEmpty(queryParams.adr)) {
            const indexController = this.controllerFor('index/index');
            // RESTORE
            this.notifications.error(
              'Disculpe, no fué posible encontrar la dirección',
              NOTIFICATION_OPTIONS
            );

            // If exists, restore a previous successfull meta
            if(!isEmpty(indexController) && get(indexController, 'loadedMap')) {
              return this.lastResults;
            }

            queryParams.adr = null;
            // If there is no previous metas, reset queryParams and reload page
            return this.transitionTo(this.urlTargetName, { queryParams });
          }
          set(this, 'lastResults', buttonsResponse);
          return buttonsResponse;
        });

      return hash({
        buttons,
        currentUser: this.currentUser,
      });
    },

    setupController(controller, model) {
      this._super(controller, model);
      set(this, 'controller.buttonsFiltered', null);
    },

    actions: {
      loading(transition) {
        const controller = this.controllerFor('index');
        controller.set('loading', true);
        transition.promise.finally(()=>{
          controller.set('loading', false);
        });
      },
    },
  }
);
