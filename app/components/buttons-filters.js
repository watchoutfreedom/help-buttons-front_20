import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { or, notEmpty } from '@ember/object/computed';
import { get, set, setProperties, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { A } from '@ember/array';

const sortOptions = ['Últimos creados', 'Primeros creados'];

export default Component.extend({
  store: service(),
  classNames: ['container-fluid', 'buttons-filters'],

  routing: service('-routing'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  userMarkerClass: computed('userMarker', function() {
    return get(this, 'userMarker') ?
      'btn btn-link search-map__localize-user search-map__localize-user--selected' :
      'btn btn-link search-map__localize-user';
  }),

  isMapRoute: computed('routing.currentRouteName', function() {
    const currentRouteName = get(this, 'routing.currentRouteName');
    if (currentRouteName === 'index.index.index') {
      return true;
    }
    return false;
  }),

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  isIndexRoute: null,
  resetFilters: null,

  // To apply filters on map move
  didUpdateAttrs() {
    if (!this.isShowingModal && this.buttons !== undefined) {
      !this.lockFilters ? set(this, 'lockFilters', true) : set(this, 'lockFilters', false);
    }
  },

  didRender() {
    if (this.resetFilters) {
      setProperties(this,
        {
          needFilter: false,
          offerFilter: false,
          resetFilters: false,
        });
    }
    if (!this.lockFilters && this.buttons !== undefined) {
      this.send('applyFilters');
    }

    const vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  },

  lockFilters: true,

  sortOptions: sortOptions,
  selectedSort: sortOptions.get('firstObject'),

  buttons: null,

  needFilter: false,
  offerFilter: false,

  isSearching: false,
  searchAction: null,
  inputAddress: null,

  isShowingModal: null,

  buttonsFiltered: null,

  isProfileList: null,
  /**
   * received param to hide the icon link to go to the search page
   * @type {Boolean}
   */
  hideMapLink: false,

  showAddressSearch: null,

  searchedTags: A([]),
  selectedFilterTags: A([]),

  hasSelectedTags: notEmpty('selectedTags'),

  updateSelectedTagsAction() {},
  toggleSelectedTagAction() {},
  localizeUser() {},

  actions: {
    searchAndFilter() {
      set(this, 'isSearching', true);
      if (!isEmpty(this.inputAddress)) {
        this.send('searchByAddress');
      }
      this.send('applyFilters');
      set(this, 'isSearching', false);
    },

    applyFilters(varName, selectOption) {
      if (!isEmpty(varName)) {
        (!isEmpty(selectOption) && typeof selectOption === 'string') ?
          set(this, varName, selectOption) : this.toggleProperty(varName);
      }
      const needFilter = this.needFilter;
      const offerFilter = this.offerFilter;
      const buttons = this.buttons;
      this.updateSelectedTagsAction(this.selectedTags);
      let filteredButtons = null;

      // FILTER BY TYPE
      if (needFilter && offerFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'change');
      } else if (needFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'need').concat(buttons.filterBy('buttonType', 'change'));
      } else if (offerFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'offer').concat(buttons.filterBy('buttonType', 'change'));
      } else {
        filteredButtons = buttons;
      }

      // SORT
      filteredButtons = filteredButtons.sortBy('createdAt');
      if (this.selectedSort === 'Últimos creados') {
        filteredButtons = filteredButtons.reverse();
      }
      set(this, 'buttonsFiltered', filteredButtons);

      if (this.isShowingModal) {
        this.send('toggleModal');
      }
    },

    cleanFilters() {
      setProperties(this, {
        'needFilter': false,
        'offerFilter': false,
        'selectedSort': sortOptions.get('firstObject'),
      });
    },
    toggleModal: function() {
      this.toggleProperty('isShowingModal');
    },
    searchByAddress() {
      const address = this.inputAddress;
      return this.searchAction(address)
        .then(() => {
          setProperties(this, {
            'isShowingModal': false,
            'inputAddress': null,
          });
        });
    },

    searchTags(term, selectClass) {
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    changeSelectedTags(tags) {
      set(this, 'selectedTags', tags);
    },

    removeTag(tag) {
      const target = get(this, 'selectedTags');
      target.removeObject(tag);
    },

    tagSuggestion() {
      return '';
    },

    closedToggleSelectedTag([ tag ]) {
      this.toggleSelectedTagAction(tag);
    },
    localizeUser() {
      return this.localizeUser();
    },
  },
});
