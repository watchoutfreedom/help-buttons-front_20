import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, get } from '@ember/object';

export default Component.extend({
  classNames: ['d-flex', 'justify-content-center', 'new-button-header'],
  routing: service('-routing'),

  isNewButton: null,

  headerBackground: null,

  headerTitle: computed(
    'routing.currentRouteName',
    'isNewButton',
    function() {
      const currentRouteName = get(this, 'routing.currentRouteName');
      const routeNameSubstr = currentRouteName.substr(currentRouteName.lastIndexOf('.'), currentRouteName.length);
      if (this.isNewButton) {
        if (routeNameSubstr.includes('index')) {
          return 'CREAR BOTÓN';
        } else if (routeNameSubstr.includes('description')) {
          return 'CREAR BOTÓN 1/3';
        } else if (routeNameSubstr.includes('location')) {
          return 'CREAR BOTÓN 2/3';
        } else if (routeNameSubstr.includes('date')) {
          return 'CREAR BOTÓN 3/3';
        } else if (routeNameSubstr.includes('activate-button')) {
          return 'VISTA PREVIA';
        } else if (routeNameSubstr.includes('login')) {
          return 'INICIA SESIÓN';
        } else if (routeNameSubstr.includes('register')) {
          return '¡ÚLTIMO PASO!';
        } else if (routeNameSubstr.includes('share')) {
          return '¡BOTÓN PUBLICADO!';
        }
        return 'CREAR BOTÓN';
      }
      if (routeNameSubstr.includes('index')) {
        return 'EDITAR BOTÓN';
      } else if (routeNameSubstr.includes('description')) {
        return 'EDITAR BOTÓN 1/3';
      } else if (routeNameSubstr.includes('location')) {
        return 'EDITAR BOTÓN 2/3';
      } else if (routeNameSubstr.includes('date')) {
        return 'EDITAR BOTÓN 3/3';
      } else if (routeNameSubstr.includes('activate-button')) {
        return 'VISTA PREVIA';
      } else if (routeNameSubstr.includes('login')) {
        return 'INICIA SESIÓN';
      } else if (routeNameSubstr.includes('register')) {
        return '¡ÚLTIMO PASO!';
      } else if (routeNameSubstr.includes('share')) {
        return '¡BOTÓN PUBLICADO!';
      }
      return 'EDITAR BOTÓN';
    }
  ),

  previousStepRouteName: computed('routing.currentRouteName', function() {
    const currentRouteName = get(this, 'routing.currentRouteName');
    const routeNameSubstr = currentRouteName.substr(0, currentRouteName.lastIndexOf('.'));
    switch (currentRouteName) {
      case routeNameSubstr + '.index':
        return 'index.index.index';
      case routeNameSubstr + '.description':
        return routeNameSubstr + '.index';
      case routeNameSubstr + '.location':
        return routeNameSubstr + '.description';
      case routeNameSubstr + '.date':
        return routeNameSubstr + '.location';
      case routeNameSubstr + '.activate-button':
        return routeNameSubstr + '.date';
      case routeNameSubstr + '.login':
        return routeNameSubstr + '.register';
      case routeNameSubstr + '.register':
        return routeNameSubstr + '.date';
      case routeNameSubstr + '.share':
        return routeNameSubstr + '.activate-button';
      default:
        return routeNameSubstr + '.index';
    }
  }),

  isShareRoute: computed('routing.currentRouteName;', function() {
    const currentRouteName = get(this, 'routing.currentRouteName');
    return currentRouteName === 'index.index.new-button.share';
  }),

  closeRouteName: computed('routing.currentRouteName', function() {
    return get(this, 'routing.currentRouteName').includes('index.list') ? 'index.list' : 'index.index';
  }),
});
