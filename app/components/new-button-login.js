import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { set, getProperties, computed } from '@ember/object';
import { notEmpty } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';
import { isEmpty } from '@ember/utils';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Component.extend({
  classNames: ['container-fluid', 'login-component'],
  classNameBindings: ['loginComponentBindClass'],
  session: service('session'),
  notifications: service('notification-messages'),
  routing: service('-routing'),
  screens: service('screen'),

  isModal: null,

  isMobileButtonCreation: null,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  // svgMaxHeight: computed(
  //   'screens.width',
  //   'screens.height',
  //   'didRenderPage',
  //   function() {
  //     this.screens.width;
  //     this.screens.height;
  //     debugger
  //     let bodyHeight = document.getElementsByClassName('login-component__modal-content')[0];
  //     let contentHeight = document.getElementById('typeContent');
  //     if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
  //       bodyHeight = bodyHeight.clientHeight;
  //       contentHeight = contentHeight.clientHeight;
  //       let svgStyles;
  //       if (this.isModal || this.screens.width < 992) {
  //         svgStyles = bodyHeight - contentHeight - 48;
  //       } else {
  //         svgStyles = this.screens.height - contentHeight - 16 - 75;
  //       }
  //       if (svgStyles < 100) {
  //         svgStyles = 100;
  //       } else if (svgStyles > 350) {
  //         svgStyles = 300;
  //       }
  //       svgStyles = `height: ${svgStyles}px;`;
  //       return htmlSafe(svgStyles);
  //     }
  //     return htmlSafe(`height: ${100}px;`);
  //   }
  // ),

  loginComponentBindClass: computed(
    'isModal',
    'isMobileButtonCreation',
    function() {
      if (this.isModal) {
        return 'login-component__modal';
      }
      if (this.isMobileButtonCreation) {
        return 'login-component--taller';
      }
      return 'login-component__main-page';
    }
  ),

  email: null,
  password: null,
  errorMessages: null,

  modalClasses: computed('isModal', function() {
    return this.isModal ? 'col-10' : 'col-12 col-sm-10 col-md-7 col-lg-9';
  }),

  hasError: notEmpty('errorMessages'),

  isVisiblePassword: false,

  routeName: null,

  passwordInputType: computed(
    'isVisiblePassword',
    function() {
      return this.isVisiblePassword ? 'text' : 'password';
    }
  ),

  actions: {
    authenticate() {
      const { email, password } = getProperties(this, 'email', 'password');
      set(this, 'errorMessage', null);
      return this.session
        .authenticate('authenticator:tiddle', email, password)
        .then(() => {
          this.notifications.success(
            'Bienvenid@ a Helpbuttons!',
            NOTIFICATION_OPTIONS
          );
        })
        .catch(({ payload }) => set(this, 'errorMessages', payload.errors) );
    },
    togglePasswordVisibily() {
      this.toggleProperty('isVisiblePassword');
    },
  },
});
