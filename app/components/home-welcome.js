import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
import { set } from '@ember/object';

export default Component.extend({
  classNames: ['home-welcome'],
  session: service('session'),
  routing: service('-routing'),

  init() {
    this._super(...arguments);
    set(this, 'welcomeTitle', this.titlesArrays[Math.floor(Math.random() * this.titlesArrays.length)]);
  },
  welcomeTitle: null,

  titlesArrays: A([
    '"¿Comprendes la filosofía no?"',
    '"Equilicuá"',
    '"La juventud está preparadísima"',
    '"Vamos, que la he liao parda"',
    '"Sa matao Paco"',
    '"Comprendes la filosofía no?"',
    '"Madre mía cómo está la vida"',
    '"El ser humano… es extraordinario"',
    'Ser en acto',
    '"Vais a vuestra bola"',
    '"Mi marido no es curioso"',
    '"Vivís en Matrix"',
    '"La herramienta para la vida colaborativa"',
  ]),

});
