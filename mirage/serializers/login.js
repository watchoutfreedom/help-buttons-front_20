import { ActiveModelSerializer } from 'ember-cli-mirage';

export default ActiveModelSerializer.extend({
  serialize(response) {
    const json = JSON.stringify({
      email: response.email,
      authentication_token: response.token,
    });

    return json;
  },
});
