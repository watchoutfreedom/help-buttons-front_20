import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | index/index/new-button/date', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const route = this.owner.lookup('route:index/index/new-button/date');
    assert.ok(route);
  });
});
