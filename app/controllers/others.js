import Controller from '@ember/controller';
import { resolve } from 'rsvp';

export default Controller.extend({
  queryParams: ['section'],
  section: null,

  actions: {
    searchAddress(address, tags) {
      debugger
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: tags,
      };
      // return resolve().then(() => {
      this.transitionToRoute('index', { queryParams });
      // });
    }
  }
});
