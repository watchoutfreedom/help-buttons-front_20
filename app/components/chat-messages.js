import Component from '@ember/component';
import { set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['chat-messages col-12'],
  scrollBottom: null,
  messages: null,
  otherUser: null,
  currentUser: null,

  otherUserAvatar: computed(
    'otherUser',
    {
      get() {
        if (!isEmpty(this.otherUser)) {
          this.otherUser.avatar.then((avatar) => {
            set(this, 'otherUserAvatar', avatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),
});
