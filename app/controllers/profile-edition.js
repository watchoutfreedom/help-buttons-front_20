import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed, set, get, setProperties } from '@ember/object';
import { alias, notEmpty } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import { resolve, hash } from 'rsvp';

const languageOptions = ['Español', 'Ingles'];

export default Controller.extend({
  session: service(),
  geolocation: service('geolocation'),

  currentUser: alias('model.currentUser'),
  avatar: alias('model.avatar'),
  selectedInterests: alias('model.interests'),
  languages: alias('model.languages'),
  selectedLanguages: alias('model.selectedLanguages'),

  interestsSaved: true,
  changedInterests: computed('interestsSaved', 'selectedInterests', function() {
    if (this.interestsSaved) {
      set(this, 'interestsSaved', false);
      return false;
    }
    return true;
  }),

  languagesSaved: true,
  changedLanguages: computed('languagesSaved', 'selectedLanguages', 'languages', function() {
    if (this.languagesSaved) {
      set(this, 'languagesSaved', false);
      return false;
    }
    return true;
  }),

  isShowingModal: false,

  showProfileEdition: false,
  showPasswordEdition: false,
  showSecurityAndPrivacity: false,
  showNotificationsSettings: false,

  languageOptions: languageOptions,

  // FIX IT
  updateUserDisabled: computed(
    'currentUser.avatarFile',
    'changedInterests',
    'changedLanguages',
    'currentUser.nickname',
    'currentUser.name',
    'currentUser.email',
    'currentUser.phone',
    'currentUser.description',
    'currentUser.location',
    'currentUser.appLanguage',
    'currentUser.active',
    'currentUser.showPhone',
    function() {
      return (!this.currentUser.hasDirtyAttributes)
      && isEmpty(this.currentUser.avatarFile)
      && !this.changedInterests
      && !this.changedLanguages;
    }
  ),

  changePasswordDisabled: computed(
    'currentUser.actualPassword',
    'currentUser.newPassword',
    'currentUser.passwordConfirmation',
    function() {
      return isEmpty(this.currentUser.currentPassword) ||
        isEmpty(this.currentUser.password) ||
        isEmpty(this.currentUser.passwordConfirmation);
    }
  ),

  hasPendingAvatar: notEmpty('currentUser.avatarFile'),

  isSharingLocation: computed('currentUser', function() {
    return !isEmpty(this.currentUser.location);
  }),

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  actions: {
    searchByAddress(address, tags) {
      debugger
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: tags,
      };
      // return resolve().then(() => {
      this.transitionToRoute('index', { queryParams });
      // });
    },

    toggleProperty(param) {
      if (param === 'showSecurityAndPrivacity' || param === 'showNotificationsSettings') {
        window.alert('Funcionalidad en desarrollo');
      } else {
        this.toggleProperty(param);
      }
    },

    updateUserProfile() {
      set(this, 'currentUser.interests', this.selectedInterests);
      set(this, 'currentUser.languages', this.selectedLanguages);
      isEmpty(this.selectedInterests) ? resolve() : this.currentUser.updateRelationship('interests');
      isEmpty(this.selectedLanguages) ? resolve() : this.currentUser.updateRelationship('languages');
      return this.currentUser.save().then((user) => {
        let image = resolve();
        if (!isEmpty(user.avatarFile)) {
          const options = {};
          options.headers = {
            'Authorization': this.session.data.authenticated.token,
          };
          options.data = {
            name: user.avatarFile.name,
            file: user.avatarFile,
            'imageable-id': user.internalId,
            'imageable-type': 'user',
            'imageable-property': 'avatar',
          };

          image = user.avatarFile.upload('/api/v1/images', options)
            .then((data) => {
              const normalizedImageResponse = this.store.normalize('image', data.body.data);
              const newImage = this.store.push(normalizedImageResponse);
              setProperties(user, {
                'avatar': newImage,
                'avatarFile': null,
              });
              return newImage;
            });
        }
        set(this, 'interestsSaved', true);
        set(this, 'languagesSaved', true);
        return hash({
          image,
        });
      });
    },

    toggleShowPhone() {
      this.toggleProperty('currentUser.showPhone');
    },

    searchTags(term) {
      return this.store.query('tag', { 'filter[name]': term })
        .then((tags) => {
          return tags.filter((tag) => !this.selectedInterests.includes(tag));
        });
    },
    searchLanguages(term) {
      return this.store.query('language', { 'filter[name]': term })
        .then((languages) => {
          return languages.filter((lang) => !this.selectedLanguages.includes(lang));
        });
    },
    changePassword() {
      set(this, 'currentUser.actionEvent', 'updatePassword');
      this.currentUser.save().then(() => {
      });
    },
    shareLocation(isSharingLocation) {
      this.toggleProperty(isSharingLocation);
      if (this.isSharingLocation) {
        set(this, 'showSpinner', true);
        this.geolocation
          .getCurrentPosition()
          .then((coordinates) => {
            const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            coordinates.latitude + ',' + coordinates.longitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
            $.getJSON(geocodingAPI).then((json) => {
              if (json.status === 'OK') {
                const result = json.results[0];
                let state = '';
                for (let i = 0, len = result.address_components.length; i < len; i++) {
                  const ac = result.address_components[i];
                  if (ac.types.indexOf('administrative_area_level_1') >= 0) state = ac.short_name;
                }
                if (state !== '') {
                  set(this, 'currentUser.location', state);
                  set(this, 'showSpinner', false);
                }
              }
            });
          });
      } else {
        set(this, 'currentUser.location', null);
      }
    },
    uploadImage(file) {
      set(this, 'currentUser.avatarFile', file);
      file.readAsDataURL().then(function(url) {
        file.set('url', url);
      });
    },

    createNewTag(targetRelationship, tagName) {
      return get(this, 'store').createRecord('tag', {
        name: tagName,
      })
        .save()
        .then((tag) => {
          get(this, targetRelationship).pushObject(tag);
        });
    },
    checkTagAlreadyExists(term) {
      const tags = this.tags;
      return !(!isEmpty(tags) && !isEmpty(tags.findBy('name', term)));
    },
    tagSuggestion(term) {
      return `Pulsa intro para crear ${term}`;
    },
    removeTag(targetRelationship, tag) {
      const target = get(this, targetRelationship);
      target.removeObject(tag);
    },
  },
});
