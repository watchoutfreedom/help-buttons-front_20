import Model from 'ember-data/model';
import ModelMixin from 'ember-data-updating-json-api-relationships/mixins/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import {  computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Model.extend(
  ModelMixin,
  {
    email: attr('string'),
    nickname: attr('string', { defaultValue: 'Sin nombre' }),
    internalId: attr('number'),

    location: attr('string'),
    description: attr('string'),
    ownedButtonsCounter: attr('number'),
    // Vitual attribute so we can authenticate when registering
    randomPassword: attr('string'),

    // Attribute when changing password
    currentPassword: attr('string'),
    password: attr('string'),
    passwordConfirmation: attr('string'),
    name: attr('string'),
    phone: attr('string'),
    showPhone: attr('boolean', { defaultValue: false }),
    appLanguage: attr('string'),
    active: attr('boolean'),

    avatar: belongsTo('image', { inverse: null }),

    ownedButtons: hasMany('button', { inverse: null }),
    buttons: hasMany('button'),
    interests: hasMany('tag', { inverse: null }),
    languages: hasMany('language', { inverse: null }),

    hasAvatar: computed(
      'avatar',
      function() {
        return !isEmpty(this.belongsTo('avatar').id());
      }
    ),

    capitalizedName: computed(
      'nickname',
      function() {
        return this.nickname.capitalize();
      },
    ),
  }
);
