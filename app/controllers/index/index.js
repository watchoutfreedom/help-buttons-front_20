import Controller, { inject as controller } from '@ember/controller';
import { get, set, computed, setProperties } from '@ember/object';
import { debounce } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { readOnly, or, alias } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';

export default Controller.extend({
  routing: service('-routing'),
  session: service(),
  mediaQueries: service(),
  geolocation: service('geolocation'),
  headTagsService: service('head-tags'),
  firstLoad: false,

  queryParameters: {
    buttonsDisplay: '',
  },

  init() {
    this._super(...arguments);
    this.addObserver('buttonsDisplay', this, 'buttonsDisplayParamObserver');
  },

  buttonsDisplayParamObserver() {
    this.get('headTagsService').collectHeadTags();
  },


  selectedSort: null,

  bounds: null,
  loadedMap: false,
  lastZoom: null,

  userMarkerClass: computed('userMarker', function() {
    return get(this, 'userMarker') ?
      'btn btn-link index-map__localize-user index-map__localize-user--selected' :
      'btn btn-link index-map__localize-user';
  }),

  disableQueryParamsSetting: false,

  storedTransition: null,

  zoomChanged: null,

  buttonsDisplay: readOnly('indexController.buttonsDisplay'),

  userMarker: readOnly('indexController.userMarker'),

  selectedTags: readOnly('indexController.selectedTags'),

  buttonsToShow: readOnly('indexController.buttonsToShow'),

  userPosition: alias('indexController.userPosition'),

  currentUser: readOnly('model.currentUser'),

  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  searchMapStyles: computed(
    'selectedTags.[]',
    'isSmallDevice',
    function() {
      const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
      const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
      const headerSectionHeight = headerHeight + searchTopBarHeight;
      const searchMapStyles = `height: ${window.innerHeight - headerSectionHeight}px; top: ${headerSectionHeight}px;`;
      return htmlSafe(searchMapStyles);
    }
  ),

  /**
   * Search type (search by area, point or address) according with transition query params
   * @type {String}
   * @property transitionType
   */
  transitionType: computed(
    'storedTransition.queryParams.{latitude,longitude,address,northEastLat,northEastLng,southWestLat,southWestLng}',
    function() {
      if(!isEmpty(this.storedTransition) && !isEmpty(this.storedTransition.queryParams)) {
        const queryParams = get(this, 'storedTransition.queryParams');
        if(!isEmpty(queryParams.nel) &&
          !isEmpty(queryParams.nelng) &&
          !isEmpty(queryParams.swl) &&
          !isEmpty(queryParams.swlng)) {
          return 'areaType';
        } else if(!isEmpty(queryParams.adr)) {
          return 'addressType';
        } else if(!isEmpty(queryParams.lat) &&
          !isEmpty(queryParams.lng)) {
          return 'pointType';
        }
        return 'errorType';
      }
      return null;
    }
  ),
  /**
   * Method to trigger a search by area, setting others QPs to null and sending bounds only,
   * @method _triggerSearchArea
   * @param  {L.LatLngBounds}           mapBounds
   * @return {Void}
   */
  _triggerSearchArea(mapBounds) {
    // const tagsParams = get(this, 'storedTransition.queryParams');
    let queryParams;
    if (this.firstLoad) {
      queryParams = {
        latitude: null,
        longitude: null,
        address: null,
        northEastLat: mapBounds._northEast.lat,
        northEastLng: mapBounds._northEast.lng,
        southWestLat: mapBounds._southWest.lat,
        southWestLng: mapBounds._southWest.lng,
        searchTags: this.selectedTags,
      };
    } else {
      set(this, 'firstLoad', true);
      queryParams = {
        latitude: null,
        longitude: null,
        address: null,
        northEastLat: mapBounds._northEast.lat,
        northEastLng: mapBounds._northEast.lng,
        southWestLat: mapBounds._southWest.lat,
        southWestLng: mapBounds._southWest.lng,
      };
    }
    // Avoid redirect to map at button route
    if (this.routing.currentRouteName === 'index.index.index') {
      this.transitionToRoute('index', { queryParams });
    } else {
      if (!this.isButtonRoute) {
        if (isEmpty(this.storedTransition) || isEmpty(this.storedTransition.intent)) {
          this.transitionToRoute(this.storedTransition, { queryParams });
        } else {
          this.transitionToRoute(this.storedTransition.intent.name, { queryParams });
        }
      }
    }
  },

  /**
   * Replace and store new transitions, it is only called from beforeModel hook.
   * @method _replaceTransition
   * @param  {Object}           transition Ember Transition Object
   * @return {Void}
   */
  _replaceTransition(transition) {
    const storedTransition = this.storedTransition;
    if(!isEmpty(storedTransition) && !isEmpty(storedTransition.intent) && storedTransition.isActive) {
      storedTransition.abort();
    }
    if (!isEmpty(transition.intent.name)) {
      set(this, 'storedTransition', transition);
    } else {
      set(this, 'storedTransition', transition.targetName);
    }
  },

  showUserPosition: computed(
    'userPosition',
    'userMarker',
    function() {
      return !isEmpty(this.userPosition) && this.userMarker;
    }
  ),

  minZoom: computed(
    function() {
      return this.isSmallDevice ? 1 : 3;
    }
  ),

  startFitBounds: false,
  resetFitBounds: false,

  isShowingModal: false,

  indexController: controller('index'),

  modalButton: null,

  isButtonRoute: computed('routing.currentRouteName', function() {
    return get(this, 'routing.currentRouteName') === 'index.index.button';
  }),

  /**
   * Indicates that the route is in loading state.
   * Derived from `search` controller.
   *
   * @property loading
   * @type {Boolean}
   */
  loading: readOnly('indexController.loading'),

  actions: {
    /**
     * Action triggered by Leaflet map when it is loaded.
     * @method loadMap
     * @return {[type]} [description]
     */
    loadMap(e) {
      set(this, 'loadedMap', true);
      const buttonsToShow = this.buttonsToShow;
      const map = e.target;
      const btnArray = [];
      buttonsToShow.forEach((model) => {
        btnArray.push(L.marker([model.location[0], model.location[1]]));
      });
      let group;
      if (btnArray.length > 1) {
        group = L.featureGroup(btnArray);
        map.fitBounds(L.latLng(group.getBounds().getNorthEast(), group.getBounds().getSouthWest()).toBounds(430000));
      } else {
        map.fitBounds(L.latLng(this.bounds.getNorthEast(), this.bounds.getSouthWest()).toBounds(430000));
      }
      set(this, 'resetFitBounds', true);
    },
    /**
     * Action triggered by map each time that map center changes
     * @method mapMoveEnd
     * @param  {Event Object}   e Event
     * @return {Void}     [description]
     */
    mapMoveEnd(e) {
      const map = e.target;

      // L.LatLngBounds
      const zoomChanged = this.zoomChanged;
      const mapBounds = map.getBounds();
      // Check if loaded, and if disableQueryParamsSetting flag is activated or coordinates changed.
      if(this.loadedMap &&
        !this.disableQueryParamsSetting &&
        (parseFloat(this.northEastLat) !== mapBounds._northEast.lat ||
        parseFloat(this.northEastLng) !== mapBounds._northEast.lng)
      ) {
        // debounce triggers the passed function after 1000ms
        // whithout execute other debounce with the same method.
        if (!zoomChanged) {
          debounce(this, this._triggerSearchArea, mapBounds, 1000);
        }
        setProperties(this, {
          'lastZoom': map.getZoom(),
          'zoomChanged': false,
        });
      }
      set(this, 'disableQueryParamsSetting', false);
    },

    mapZoomEnd(e) {
      const lastZoom = this.lastZoom;
      if (!isEmpty(lastZoom)) {
        e.target.getZoom() > lastZoom ? set(this, 'zoomChanged', true) : set(this, 'zoomChanged', false);
      } else {
        set(this, 'lastZoom', e.target.getZoom());
      }
    },
    markerClick(id) {
      this.transitionToRoute('index.index.button.index', id);
    },

    localizeUser() {
      if(get(this, 'userMarker')) {
        const queryParams = {
          latitude: get(this, 'userPosition.lat'),
          longitude: get(this, 'userPosition.lng'),
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: false,
        };
        this.transitionToRoute('index', { queryParams });
        return null;
      }
      const geolocation = get(this, 'geolocation');
      return geolocation.getCurrentPosition()
        .then((coordinates) => {
          const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);

          set(this, 'userPosition', userPosition);

          const queryParams = {
            latitude: userPosition.lat,
            longitude: userPosition.lng,
            address: null,
            northEastLat: null,
            northEastLng: null,
            southWestLat: null,
            southWestLng: null,
            userMarker: true,
          };
          this.transitionToRoute('index', { queryParams });
        });
    },
  },
});
