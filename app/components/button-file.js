import Component from '@ember/component';
import { set, get, computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { or, readOnly } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Component.extend({
  session: service(),
  notifications: service('notification-messages'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  routing: service('-routing'),


  buttonIsMine: computed('button.isMine', function() {
    return get(this, 'button.isMine');
  }),
  button: null,
  closeAction: null,

  chatsComp: computed(
    'button',
    'button.chats',
    'buttonIsMine',
    {
      get() {
        debugger
        if (get(this, 'button.isMine')) {
          debugger
          get(this, 'button.chats').then((chats) => {
            debugger
            set(this, 'chatsComp', chats);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  // chatsComp: computed('button', 'button.chats', 'buttonIsMine', function() {
  //   debugger
  //   if (get(this, 'button.isMine')) {
  //     return get(this, 'button.chats').then((chats) => {
  //       debugger
  //       return chats;
  //     });
  //   }
  //   return null;
  // }),
  routeType: null,
  showProfile: null,
  showModalButton: false,
  currentUser: readOnly('session.currentUser'),

  bigButtonAction: computed('button', 'button.isMine', function() {
    return get(this, 'button.isMine') ? 'toggleButtonState' : 'openConversation';
  }),

  showConversations: false,

  svgIcon: computed('button.{isMine,active}', function() {
    if (get(this, 'button.isMine')) {
      return 'switch';
    }
    return 'chat';
  }),

  buttonClass: computed('button.{isMine,active}', function() {
    const button = this.button;

    return button.isMine && !button.active ?
      'button-file__yellow-circle--inactive-button' : '';
  }),

  creatorName: computed(
    'button',
    {
      get() {
        const button = get(this, 'button');
        get(button, 'creator')
          .then((creator) => {
            set(this, 'creatorName', creator.nickname);
          });
      },
      set(key, value) {
        return value;
      },
    }
  ),

  allowEdit: computed('routing.currentRouteName', function() {
    return this.routing.currentRouteName === 'profile.buttons';
  }),

  openConversation: null,
  removeButtonTransition: null,
  showButtonFile: true,
  buttonCardClick: false,

  socialShareTransition: null,
  socialShareURL: null,

  actions: {
    markerClick() {
      this.closeAction();
    },

    openConversation() {
      this.openConversation();
    },

    toggleButtonState() {
      const button = get(this, 'button');
      this.toggleProperty('button.active');
      return button.save().then((btn) => {
        if (btn.active) {
          this.notifications.success(
            'Has activado el botón',
            NOTIFICATION_OPTIONS,
          );
        } else {
          this.notifications.success(
            'Has desactivado el botón',
            NOTIFICATION_OPTIONS,
          );
        }
      });
    },

    toggleProperty(param) {
      this.toggleProperty(param);
    },
    removeButtonTransition() {
      this.removeButtonTransition();
    },
    closeButtonFile() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
  },
});
